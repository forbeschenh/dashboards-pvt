package nacpPageObjects;

import org.openqa.selenium.WebDriver;

import basePageObjects.BasicPageObjects;

/**
Author: dparakrama
Created : Mar 20, 2018
 */
public class NapCResultsUIElementsPage extends BasicPageObjects {

	String btnResults = ".//*[@id='workflow-progress-results']";
	
	String lnkSchoolAndStudentReports = ".//*[@id='dashboard-panel-results-school-student-reports']/div[1]/h3";
	String lnkSSR = ".//*[@id='dashboard-panel-results-school-student-reports']/div[2]/span";
	String lnkReports = ".//*[@id='dashboard-panel-results-reports']/div[1]/h3";
	
	String lnkR1 = ".//*[@id='dashboard-panel-results-reports']/div[2]/a[1]";
	String lnkR2 = ".//*[@id='dashboard-panel-results-reports']/div[2]/a[2]";
	String lnkR3 = ".//*[@id='dashboard-panel-results-reports']/div[2]/a[3]";
	String lnkR4 = ".//*[@id='dashboard-panel-results-reports']/div[2]/a[4]";
	String lnkR5 = ".//*[@id='dashboard-panel-results-reports']/div[2]/a[5]";
	String lnkR6 = ".//*[@id='dashboard-panel-results-reports']/div[2]/a[6]";
	
	
	
	public NapCResultsUIElementsPage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	
	public void verifyPageElements () throws InterruptedException{
		clickButton(btnResults);
		/*isElementPresent(lnkSchoolAndStudentReports,
				"School and Student Reports");*/
		isElementPresent(lnkSSR,
				"Download School and Student Summary Report (SSSR)");
		/*isElementPresent(lnkReports,
				"Reports");*/
		
		isElementPresent(lnkR1,
				"Open Test Session List");
		isElementPresent(lnkR2,
				"Test Disruption");
		isElementPresent(lnkR3,
				"Test Session Finalisation");
		isElementPresent(lnkR4,
				"Multi school student and student information QA");
		isElementPresent(lnkR5,
				"Hosted and Visiting Students");
		isElementPresent(lnkR6,
				"Excluded Students");
		
	}
}

