package taaPageObjects;

import org.openqa.selenium.WebDriver;

import basePageObjects.BasicPageObjects;



public class TAAAllDeliveringUIElementsPage extends BasicPageObjects {
	String lnkchecklistElement1 = "//*[@id='dashboard-panel-registration-checklist']/div[2]/div/div/div/div/p[1]/a/span[2]";
	String lnkchecklistElement2 = "//*[@id='dashboard-panel-registration-checklist']/div[2]/div/div/div/div/p[2]/a/span[2]";
	String lnkchecklistElement3 = "//*[@id='dashboard-panel-registration-checklist']/div[2]/div/div/div/div/p[3]/a/span[2]";

	String lnkMonitorAssessmentDeliveryElement1 = "//*[@id='dashboard']/div[2]/div/div[4]/div[2]/div[2]/div/div/div/div/div[1]/div[2]/a[1]";
	String lnkMonitorAssessmentDeliveryElement2 = "//*[@id='dashboard']/div[2]/div/div[4]/div[2]/div[2]/div/div/div/div/div[1]/div[2]/a[2]";

	String lnkManageStudentInformationandRecordElement1 = "//*[@id='dashboard']/div[2]/div/div[4]/div[2]/div[2]/div/div/div/div/div[2]/div[2]/a[1]";
	String lnkManageStudentInformationandRecordElement2 = "//*[@id='dashboard']/div[2]/div/div[4]/div[2]/div[2]/div/div/div/div/div[2]/div[2]/a[2]";
	String lnkManageStudentInformationandRecordElement3 = "//*[@id='dashboard']/div[2]/div/div[4]/div[2]/div[2]/div/div/div/div/div[2]/div[2]/a[3]";
	String lnkManageStudentInformationandRecordElement4 = "//*[@id='dashboard']/div[2]/div/div[4]/div[2]/div[2]/div/div/div/div/div[2]/div[2]/a[4]";

	String lnkReportsElement1 = "//*[@id='dashboard']/div[2]/div/div[4]/div[2]/div[2]/div/div/div/div/div[3]/div[2]/a[1]";
	String lnkReportsElement2 = "//*[@id='dashboard']/div[2]/div/div[4]/div[2]/div[2]/div/div/div/div/div[3]/div[2]/a[2]";
	String lnkReportsElement3 = "//*[@id='dashboard']/div[2]/div/div[4]/div[2]/div[2]/div/div/div/div/div[3]/div[2]/a[3]";
	String lnkReportsElement4 = "//*[@id='dashboard']/div[2]/div/div[4]/div[2]/div[2]/div/div/div/div/div[3]/div[2]/a[4]";
	String lnkReportsElement5 = "//*[@id='dashboard']/div[2]/div/div[4]/div[2]/div[2]/div/div/div/div/div[3]/div[2]/a[5]";
	String lnkReportsElement6 = "//*[@id='dashboard']/div[2]/div/div[4]/div[2]/div[2]/div/div/div/div/div[3]/div[2]/a[6]";
	String lnkReportsElement7 = "//*[@id='dashboard']/div[2]/div/div[4]/div[2]/div[2]/div/div/div/div/div[3]/div[2]/a[7]";
	String lnkReportsElement8 = "//*[@id='dashboard']/div[2]/div/div[4]/div[2]/div[2]/div/div/div/div/div[3]/div[2]/a[8]";
	String lnkReportsElement9 = "//*[@id='dashboard']/div[2]/div/div[4]/div[2]/div[2]/div/div/div/div/div[3]/div[2]/a[9]";
	String lnkReportsElement10 = "//*[@id='dashboard']/div[2]/div/div[4]/div[2]/div[2]/div/div/div/div/div[3]/div[2]/a[10]";

	String lnkTestAttempStatus = "//*[@id='dashboard-panel-submission-status-chart']/div[1]/h3";
	String lnkTestAttempStatusElement1 = ".//*[@id='test-attempt-status-chart-pie']/div/div/div/div/div[1]/span";
	String lnkTestAttempStatusElement2 = ".//*[@id='test-attempt-status-chart-pie']/div/div/div/div/div[2]/span";
	String lnkTestAttempStatusElement3 = ".//*[@id='test-attempt-status-chart-pie']/div/div/div/div/div[3]/span";
	String lnkTestAttempStatusElement4 = ".//*[@id='test-attempt-status-chart-pie']/div/div/div/div/div[4]/span";
	String lnkTestAttempStatusElement5 = ".//*[@id='test-attempt-status-chart-pie']/div/div/div/div/div[5]/span";
	String lnkTestAttempStatusElement6 = ".//*[@id='test-attempt-status-chart-pie']/div/div/div/div/div[6]/span";

	String lnkProgressByYearLevel = "//*[@id='dashboard-panel-progress-by-year-level-chart']/div[1]/h3";
	String lnkProgressByYearLevelElement1 = ".//*[@id='progress-by-year-level-chart-bar']/div/div/div/div/div[1]/span";
	String lnkProgressByYearLevelElement2 = ".//*[@id='progress-by-year-level-chart-bar']/div/div/div/div/div[2]/span";

	public TAAAllDeliveringUIElementsPage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	public void verifyChecklistItems() throws InterruptedException {
		isElementPresent(lnkchecklistElement1,
				"All alternative test materials received and processed.");
		isElementPresent(lnkchecklistElement2,
				"All test attempts have a status of submitted.");
		isElementPresent(lnkchecklistElement3,
				"All data has undergone quality checks and is ready for national reporting.");

	}

	public void verifyMonitorAssessmentDeliveryItems()
			throws InterruptedException {
		isElementPresent(lnkMonitorAssessmentDeliveryElement1,
				"Monitor school progress");
		isElementPresent(lnkMonitorAssessmentDeliveryElement2,
				"Monitor devices");
	}

	public void verifyManageStudentInformationandRecord()
			throws InterruptedException {

		isElementPresent(lnkManageStudentInformationandRecordElement1,
				"Manage student information");
		isElementPresent(lnkManageStudentInformationandRecordElement2,
				"Exclude student test attempt");
		isElementPresent(lnkManageStudentInformationandRecordElement3,
				"Transfer student test attempt");
		isElementPresent(lnkManageStudentInformationandRecordElement4,
				"View student test attempt transfer requests");

	}
     public void verifyReports() throws InterruptedException {
    	 
 		isElementPresent(lnkReportsElement1,
				"Open Test Session List");
		isElementPresent(lnkReportsElement2,
				"Test Disruption"); 
		isElementPresent(lnkReportsElement3,
				"Test Session Finalisation");		 
		isElementPresent(lnkReportsElement4,
				"Multi school student and student information QA");		
		isElementPresent(lnkReportsElement5,
				"Hosted and Visiting Students");
		isElementPresent(lnkReportsElement6,
				"Excluded Students");	 
		isElementPresent(lnkReportsElement7,
				"Student Test Attempt Transfers");	 
		isElementPresent(lnkReportsElement8,
				"Student Participation and information");	 
		isElementPresent(lnkReportsElement9,
				"Student Participation Summary"); 
		isElementPresent(lnkReportsElement10,
				"Test Session Status");	 		 
     }
     
   public void verifyTestAttemptStatusGraph () throws InterruptedException {
		isElementPresent(lnkTestAttempStatus,
				"Test Attempt Status");
		isElementPresent(lnkTestAttempStatusElement1,
				"Submitted");
		isElementPresent(lnkTestAttempStatusElement2,
				"Open");
		isElementPresent(lnkTestAttempStatusElement3,
				"Abandoned");
		isElementPresent(lnkTestAttempStatusElement4,
				"Postponed");
		isElementPresent(lnkTestAttempStatusElement5,
				"Refused");
		isElementPresent(lnkTestAttempStatusElement6,
				"Other");   
   }
    
   public void verifyProgressbyYearGraph () throws InterruptedException { 
		isElementPresent(lnkProgressByYearLevel,
				"Progress by Year Level");
		isElementPresent(lnkProgressByYearLevelElement1,
				"Enrolled");
		isElementPresent(lnkProgressByYearLevelElement2,
				"Completed"); 
   }
  
}
