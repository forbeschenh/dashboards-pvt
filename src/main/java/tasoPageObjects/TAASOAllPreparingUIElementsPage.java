package tasoPageObjects;

import org.openqa.selenium.WebDriver;

import basePageObjects.BasicPageObjects;



public class TAASOAllPreparingUIElementsPage extends BasicPageObjects {

	String lnkchecklistElement1 = "//*[@id='dashboard-panel-trt-checklist']/div[2]/div/div/div/div/p[1]/a/span[2]";
	String lnkchecklistElement2 = "//*[@id='dashboard-panel-trt-checklist']/div[2]/div/div/div/div/p[2]/a/span[2]";
	String lnkchecklistElement3 = "//*[@id='dashboard-panel-trt-checklist']/div[2]/div/div/div/div/p[3]/a/span[2]";
	String lnkchecklistElement4 = "//*[@id='dashboard-panel-trt-checklist']/div[2]/div/div/div/div/p[4]/a/span[2]";
	String lnkchecklistElement5 = "//*[@id='dashboard-panel-trt-checklist']/div[2]/div/div/div/div/p[5]/a/span[2]";
	String lnkchecklistElement6 = "//*[@id='dashboard-panel-trt-checklist']/div[2]/div/div/div/div/p[6]/a/span[2]";
	String lnkchecklistElement7 = "//*[@id='dashboard-panel-trt-checklist']/div[2]/div/div/div/div/p[7]/a/span[2]";
	String lnkchecklistElement8 = "//*[@id='dashboard-panel-trt-checklist']/div[2]/div/div/div/div/p[8]/a/span[2]";
	String lnkchecklistElement9 = "//*[@id='dashboard-panel-trt-checklist']/div[2]/div/div/div/div/p[9]/a/span[2]";
	String lnkchecklistElement10 = "//*[@id='dashboard-panel-trt-checklist']/div[2]/div/div/div/div/p[10]/a/span[2]";

	String lnkStudentregistration = "//a[contains(text(),'Student registration')]";
	
	String lnkManageUsersItem1 = "//a[contains(text(),'Invite principal (single)')]";                                  
	String lnkManageUsersItem2 = "//a[contains(text(),'Invite principals (bulk)')]";
	String lnkManageUsersItem3 = "//a[contains(text(),'Re-invite principals (bulk)')]";
	String lnkManageUsersItem4 = "//a[contains(@class,\"list-group-item element-sector-administrator-hidden element-taa-administrator-hidden element-main-role_super-admin-hidden element-main-role_super-super-admin-hidden\")]";
	String lnkManageUsersItem5 = "//a[@class='list-group-item ng-scope'][contains(text(),'Manage student information')]";
	String lnkManageUsersItem6 = "//a[contains(text(),'Create student')]";
	String lnkManageUsersItem7 = "//a[contains(text(),'Manage student participation and disability adjustments')]";
	String lnkManageUsersItem8 = "//a[contains(text(),'Invite NAPLAN Coordinator to register')]";


	String lnkManageJurusdictionInformationItem1 = "//a[contains(text(),'Manage school information')]";
	String lnkManageJurusdictionInformationItem2 = "//a[contains(text(),'Manage school PINs')]";
	

	String lnkReportsItem1 = "//a[contains(text(),'Student Disability Adjustments')]";
	String lnkReportsItem2 = "//a[contains(text(),'Exempted and Withdrawn Students')]";
	String lnkReportsItem3 = "//a[contains(text(),'Assessment Platform School Listing')]";
	String lnkReportsItem4 = "//a[contains(text(),'Technical Readiness')]";
	String lnkReportsItem5 = "//a[contains(text(),'Test Administration')]";

	String lnkSchoolTechnicalReadiness = "//*[@id='dashboard']/div[2]/div/div[3]/div[3]/div[2]/div/div/div[2]/div/div[1]/div[1]/h3";
	//String imgSchoolTechnicalReadinessGraph = "//*[@id='highcharts-0']/svg/g[1]/g[1]/path[4]";
	String lnkSchoolTechnicalReadinessGraph1 = ".//*[@id='school-technical-readiness-chart-pie']/div/div/div/div/div[1]/span";
	String lnkSchoolTechnicalReadinessGraph2 = ".//*[@id='school-technical-readiness-chart-pie']/div/div/div/div/div[2]/span";
	String lnkSchoolTechnicalReadinessGraph3 = ".//*[@id='school-technical-readiness-chart-pie']/div/div/div/div/div[3]/span";
	String lnkSchoolTechnicalReadinessGraph4 = ".//*[@id='school-technical-readiness-chart-pie']/div/div/div/div/div[4]/span";

	String lnkSchoolDeliveryMode = "//*[@id='dashboard']/div[2]/div/div[3]/div[3]/div[2]/div/div/div[2]/div/div[2]/div[1]/h3";
//	String imgSchoolDeliveryModeGraph = "//*[@id='highcharts-3']/svg/g[1]/g[1]/path[4]";
	String lnkSchoolDeliveryModeGraph1 = ".//*[@id='school-delivery-mode-chart-pie']/div/div/div/div/div[1]/span";
	String lnkSchoolDeliveryModeGraph2 = ".//*[@id='school-delivery-mode-chart-pie']/div/div/div/div/div[2]/span";
	String lnkSchoolDeliveryModeGraph3 = ".//*[@id='school-delivery-mode-chart-pie']/div/div/div/div/div[3]/span";
	String lnkSchoolDeliveryModeGraph4 = ".//*[@id='school-delivery-mode-chart-pie']/div/div/div/div/div[4]/span";

	String lnkSchoolPreparationStatus = "//*[@id='dashboard']/div[2]/div/div[3]/div[3]/div[2]/div/div/div[2]/div/div[3]/div[1]/h3";
	//String imgSchoolPreparationStatusGraph = "//*[@id='highcharts-0']/svg/g[1]/g[1]/path[1]";
	String lnkSchoolPreparationStatusGraph1 = ".//*[@id='school-preparation-status-chart-pie']/div/div/div/div/div[1]/span";
	String lnkSchoolPreparationStatusGraph2 = ".//*[@id='school-preparation-status-chart-pie']/div/div/div/div/div[2]/span";
	String lnkSchoolPreparationStatusGraph3 = ".//*[@id='school-preparation-status-chart-pie']/div/div/div/div/div[3]/span";
	

	String lnkStudentParticipationStatus = "//*[@id='dashboard']/div[2]/div/div[3]/div[3]/div[2]/div/div/div[2]/div/div[4]/div[1]/h3";
	//String imgStudentParticipationStatusGraph = "//*[@id= 'highcharts-9']/svg/g[1]/g[1]/path[1]";
	String lnkStudentParticipationStatusGraph1 = ".//*[@id='student-participation-chart-pie']/div/div/div/div/div[1]/span";
	String lnkStudentParticipationStatusGraph2 = ".//*[@id='student-participation-chart-pie']/div/div/div/div/div[2]/span";
	String lnkStudentParticipationStatusGraph3 = ".//*[@id='student-participation-chart-pie']/div/div/div/div/div[3]/span";
	String lnkStudentParticipationStatusGraph4 = ".//*[@id='student-participation-chart-pie']/div/div/div/div/div[4]/span";
	String lnkStudentParticipationStatusGraph5 = ".//*[@id='student-participation-chart-pie']/div/div/div/div/div[5]/span";

	public TAASOAllPreparingUIElementsPage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	public void verifyChecklistItems() throws InterruptedException {
		isElementPresent(lnkchecklistElement1,
				"Schools ASL information is correct and complete.");
		isElementPresent(
				lnkchecklistElement2,
				"Withdrawal and exemption forms have been processed and schools informed of outcome.");
		isElementPresent(
				lnkchecklistElement3,
				"Disability adjustment requests have been processed and schools informed of outcome.");
		isElementPresent(lnkchecklistElement4,
				"Schools have completed technical readiness testing and capacity planning");
		isElementPresent(
				lnkchecklistElement5,
				"Schools assessment delivery channel (online, school test server, offline) approvals have been processed and schools have been informed of outcome.");
		isElementPresent(lnkchecklistElement6,
				"School test servers have been set-up and distributed as appropriate.");
		isElementPresent(
				lnkchecklistElement7,
				"Alternative format test materials have been ordered, received and distributed.");
		isElementPresent(
				lnkchecklistElement8,
				"All test related handbooks, information pamphlets and schedules have been reviewed and distributed.");
		isElementPresent(
				lnkchecklistElement9,
				"Schools have Principal and NAPLAN Coordinator accounts in place within the ADS.");
		isElementPresent(lnkchecklistElement10,
				"Schools have completed all tasks associated with the ‘preparing’ workflow.");
	}

	public void verifyManageUsersLinksSRMActive() throws InterruptedException {
		isElementPresent(lnkStudentregistration, "Student registration");
		isElementPresent(lnkManageUsersItem1, "Invite principal (single)");
/*		isElementPresent(lnkManageUsersItem2, "Invite principals (bulk)");
		isElementPresent(lnkManageUsersItem3, "Re-invite principals (bulk)");*/
		isElementPresent(lnkManageUsersItem4, "Manage users");
		isElementPresent(lnkManageUsersItem8, "Invite NAPLAN Coordinator to register");
	}
	
	public void verifyManageUsersLinksSRMInactive() throws InterruptedException {
		isElementPresent(lnkManageUsersItem1, "Invite principal (single)");
		/*isElementPresent(lnkManageUsersItem2, "Invite principals (bulk)");
		isElementPresent(lnkManageUsersItem3, "Re-invite principals (bulk)");*/
		isElementPresent(lnkManageUsersItem4, "Manage users");
		isElementPresent(lnkManageUsersItem5, "Manage student information");
		isElementPresent(lnkManageUsersItem6, "Create student");
		isElementPresent(lnkManageUsersItem7, "Manage student participation and disability adjustments");
		isElementPresent(lnkManageUsersItem8, "Invite NAPLAN Coordinator to register");
	}

	public void verifyManageJurisdictionInformation()
			throws InterruptedException {
		isElementPresent(lnkManageJurusdictionInformationItem1,
				"Manage school information");
		isElementPresent(lnkManageJurusdictionInformationItem2,
				"Manage school PINs");

	}

	public void verifyReports() throws InterruptedException {
		isElementPresent(lnkReportsItem1, "Student Disability Adjustments");
		isElementPresent(lnkReportsItem2, "Exempted and Withdrawn Students");
		isElementPresent(lnkReportsItem3, "Assessment Platform School Listing");
		isElementPresent(lnkReportsItem4, "Technical Readiness");
		isElementPresent(lnkReportsItem5, "Test Administration");

	}

	public void verifySchoolTechnicalReadinessGraph()
			throws InterruptedException {
		isElementPresent(lnkSchoolTechnicalReadiness,
				"School Technical Readiness");
		isElementPresent(lnkSchoolTechnicalReadinessGraph1, "Pass");
		isElementPresent(lnkSchoolTechnicalReadinessGraph2, "Incomplete");
		isElementPresent(lnkSchoolTechnicalReadinessGraph3, "Fail");
		isElementPresent(lnkSchoolTechnicalReadinessGraph4, "No Data");
	}

	public void verifySchoolDeliveryModeGraph() throws InterruptedException {
		isElementPresent(lnkSchoolDeliveryMode, "School Delivery Mode");
		isElementPresent(lnkSchoolDeliveryModeGraph1, "Other");
		isElementPresent(lnkSchoolDeliveryModeGraph2, "Offline");
		isElementPresent(lnkSchoolDeliveryModeGraph3, "Low Bandwidth");
		isElementPresent(lnkSchoolDeliveryModeGraph4, "Online");
	}

	public void verifySchoolPreparationStatusGraph()
			throws InterruptedException {
		isElementPresent(lnkSchoolPreparationStatus,
				"School Preparation Status");
	    isElementPresent(lnkSchoolPreparationStatusGraph1, "Preparing");
		isElementPresent(lnkSchoolPreparationStatusGraph2, "Delivering");
		isElementPresent(lnkSchoolPreparationStatusGraph3, "Results");

	}

	public void verifyStudentParticipationStatusGraph()
			throws InterruptedException {
		isElementPresent(lnkStudentParticipationStatus,
				"Student Participation Status");
		isElementPresent(lnkStudentParticipationStatusGraph1, "Participating");
		isElementPresent(lnkStudentParticipationStatusGraph2, "Exempt");
		isElementPresent(lnkStudentParticipationStatusGraph3, "Withdrawn");
		isElementPresent(lnkStudentParticipationStatusGraph4, "Absent");
		isElementPresent(lnkStudentParticipationStatusGraph5,
				"No longer enrolled");
	}
	
	

}
