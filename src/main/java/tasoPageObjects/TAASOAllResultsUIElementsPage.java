package tasoPageObjects;

import org.openqa.selenium.WebDriver;

import basePageObjects.BasicPageObjects;


public class TAASOAllResultsUIElementsPage extends BasicPageObjects {

	public TAASOAllResultsUIElementsPage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	String lnkResults = "//*[@id='workflow-progress-results']";
	String lnkchecklistElement1 = "//*[@id='dashboard-panel-delivery-checklist']/div[2]/div/div/div/div/p[1]/a";
	String lnkchecklistElement2 = "//*[@id='dashboard-panel-delivery-checklist']/div[2]/div/div/div/div/p[2]/a";
	String lnkDeliveryResultsItem1 = "//*[@id='dashboard']/div[2]/div/div[4]/div[4]/div[2]/div/div/div/div/div/div[2]/a[1]";
	String lnkDeliveryResultsItem2 = "//*[@id='dashboard']/div[2]/div/div[4]/div[4]/div[2]/div/div/div/div/div/div[2]/a[3]";
	String lnkDeliveryResultsItem3 = "//*[@id='dashboard']/div[2]/div/div[4]/div[4]/div[2]/div/div/div/div/div/div[2]/a[4]";
	String lnkDeliveryResultsItem4 = "//*[@id='dashboard']/div[2]/div/div[4]/div[4]/div[2]/div/div/div/div/div/div[2]/a[5]";

	public void clickResultsTab() throws InterruptedException {

		clickButton(lnkResults);

	}

	public void verifyChecklistItems() throws InterruptedException {

		isElementPresent(
				lnkchecklistElement1,
				"The school and student summary report has been reviewed and made available to schools.");
		isElementPresent(
				lnkchecklistElement2,
				"The individual student reports have been received, reviewed and distributed to schools.");
	}

	public void verifyDeleiveryResultsItems() throws InterruptedException {
		isElementPresent(lnkDeliveryResultsItem1, "Extract jurisdictional data");
		isElementPresent(lnkDeliveryResultsItem2, "Release data to ACARA");
		isElementPresent(lnkDeliveryResultsItem3,
				"Release School and Student Summary Report (SSSR)");

		
	
	}
	


}