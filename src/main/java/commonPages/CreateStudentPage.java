package commonPages;

import org.openqa.selenium.WebDriver;

import basePageObjects.BasicPageObjects;

/**
Author: dparakrama
Created : Mar 20, 2018
 */
public class CreateStudentPage extends BasicPageObjects {
	
	String btnPreparing = ".//*[@id='workflow-progress-preparing']";
	String btnCreateStudent = "//*[contains(text(), 'Create student')]";
	
	String lblHEader = ".//*[@id='content']/div[3]/h1";
    
	
	

	public CreateStudentPage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
	
	public void verifyCreateStudnetPage () throws InterruptedException {
		
		clickButton(btnPreparing);
		clickButton(btnCreateStudent);
		verifyPageTitle(lblHEader,"New Student");	
		
	}

}

