package commonPages;

import org.openqa.selenium.WebDriver;

import basePageObjects.BasicPageObjects;

/**
Author: dparakrama
Created : Mar 20, 2018
 */
public class InviteNaplanCoordinatorPage extends BasicPageObjects {
	
	String btnPreparing = ".//*[@id='workflow-progress-preparing']";
	String btnInvitePrinciple = "//*[contains(text(), 'Invite NAPLAN Coordinator to register')]";
	
	String lblHEader = ".//*[@id='content']/div[3]/h1";
	String drpSchool = ".//*[@id='s2id_OrgUnitId']/a/span";
	String txtSchool = ".//*[@id='select2-drop']/div/input";
	String btnSchoolSelection = ".//*[@id='select2-drop']/ul/li[1]/div";		
	String txtEmailAdress = ".//*[@id='EmailAddresses']";
	
	String txtEmailSubject = ".//*[@id='EmailSubject']";
	String btnSendEmail = ".//*[@id='content']/form/div[3]/div/span/button";

	public InviteNaplanCoordinatorPage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

public void verifyInviteNaplanCoordinator(String Email) throws InterruptedException {
		
		clickButton(btnPreparing);
		clickButton(btnInvitePrinciple);		
		verifyPageTitle(lblHEader,"Invite NAPLAN Coordinators via secure link");	
		clickButton(drpSchool);
		clickButton(btnSchoolSelection);		
		enterText(txtEmailAdress, Email);
		clickButton(btnSendEmail);
	}
	
	
}