package commonPages;

import org.openqa.selenium.WebDriver;

import basePageObjects.BasicPageObjects;

/**
Author: dparakrama
Created : Mar 20, 2018
 */
public class ReinviteBulkPrinciples extends BasicPageObjects{
	
	String btnPreparing = ".//*[@id='workflow-progress-preparing']";
	String btnInvitePrinciple = "//*[contains(text(), 'Re-invite principals (bulk)')]";

	String lblHEader = ".//*[@id='content']/div[3]/h1";
	String txtSubject = ".//*[@id='Subject']";
		
	String txtRole = ".//*[@id='s2id_RoleId']/a/span";
	String drpJurisdiction = ".//*[@id='s2id_OrgId']/a/span";	
	String btnResult = ".//*[@id='select2-drop']/ul/li[1]/div";	
	String btnSend = ".btn.button.btn-primary";
		
	
	public ReinviteBulkPrinciples(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	public void verifyReInvitingBulkPrinciples () throws InterruptedException {
		clickButton(btnPreparing);
		clickButton(btnInvitePrinciple);	
		verifyPageTitle(lblHEader,"Re-invite Principals");		
		getContent(txtRole, "Principal");
		clickButton(drpJurisdiction);
		clickButton(btnResult);
		clickButtonbyCss(btnSend);
	}
	
	
}

