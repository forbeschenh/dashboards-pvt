package commonPages;

import org.openqa.selenium.WebDriver;

import basePageObjects.BasicPageObjects;

/**
Author: dparakrama
Created : Mar 20, 2018
 */
public class ManageStudentParticipationPage extends BasicPageObjects {
	String btnPreparing = ".//*[@id='workflow-progress-preparing']";
	String btnManageStudentParticpation = "//*[contains(text(), 'Manage student participation and disability adjustments')]";


	String lblHEader = ".//*[@id='content']/manage-participation/header/h1";	
	String btnSearchBar = ".//*[@id='content']/div[6]/form/h3";
	
	String drpSchool = ".//*[@id='s2id_search-orgUnitId']/a/span";
	String btnSchoolResult = ".//*[@id='select2-drop']/ul/li[2]/div";
	
	String btnGroup = ".//*[@id='s2id_search-groupId']/a/span";
	String btnGroupResult = ".//*[@id='select2-drop']/ul/li[2]/ul/li[3]/div";
	
	
	String btnSearch = ".//*[@id='btnPerformSearch']";
	String txtResult = ".//*[@id='Grid_1']/table/tbody/tr[1]/td[2]/span";
	
	
	

	public ManageStudentParticipationPage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
	
	public void verifyManageStudentP () throws InterruptedException {
		
		clickButton(btnPreparing);
		clickButton(btnManageStudentParticpation);
		verifyPageTitle(lblHEader,"Manage student participation and disability adjustments");	
		
		clickButton(drpSchool);
		clickButton(btnSchoolResult);
		
		clickButton(btnGroup);
		clickButton(btnGroupResult);
		
		clickButton(btnSearch);
		Thread.sleep(4000);
		getContent(txtResult, "7");
	}

}

