package commonPages;

import org.openqa.selenium.WebDriver;

import basePageObjects.BasicPageObjects;

/**
Author: dparakrama
Created : Mar 20, 2018
 */
public class ExcludeStudentRecordPage extends BasicPageObjects {
	String btnDelivering= ".//*[@id='workflow-progress-delivering']";
	String btnExcludeRecords = "//*[contains(text(), 'Exclude student test attempt')]";


	String lblHEader = ".//*[@id='content']/div[3]/h1";	
	String btnSearchBar = ".//*[@id='content']/div[6]/form/h3";
	
	String txtSearchName = ".//*[@id='search-nameFilter']";
	String btnSearch = ".//*[@id='btnPerformSearch']";
	String txtResult = ".//*[@id='Grid_1']/table/tbody/tr[1]/td[3]/a";
	
	
	
	public ExcludeStudentRecordPage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	
	public void verifyExcludeStudents() throws InterruptedException {
		
		clickButton(btnDelivering);
		clickButton(btnExcludeRecords);
		verifyPageTitle(lblHEader,"Test Attempt");	
		
		clickButton(btnSearchBar);
		enterText(txtSearchName, "ESA");
		clickButton(btnSearch);
		getContent(txtResult, "ESA");
	}
}

