package commonPages;

import org.openqa.selenium.WebDriver;

import basePageObjects.BasicPageObjects;

public class StudentRegistration extends BasicPageObjects{
	
	String btnPreparing = ".//*[@id='workflow-progress-preparing']";
	String lnkStudentregistration = "//a[contains(text(),'Student registration')]";
	String StudentRegistrationYes = "//button[contains(text(),'Yes')]";
	String Sonet = "//*[contains(text(),'The received group')]";

	public StudentRegistration(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
	
	
	public void verifyStudentRegistrationLink() throws InterruptedException {
		
		clickButton(btnPreparing);
		clickLink(lnkStudentregistration);
		clickButton(StudentRegistrationYes);
		isElementPresent(Sonet,"");
		
	}

}
