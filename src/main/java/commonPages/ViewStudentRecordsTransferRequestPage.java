package commonPages;

import org.openqa.selenium.WebDriver;

import basePageObjects.BasicPageObjects;

/**
Author: dparakrama
Created : Mar 20, 2018
 */
public class ViewStudentRecordsTransferRequestPage extends BasicPageObjects{
	String btnDelivering= ".//*[@id='workflow-progress-delivering']";
	String btnRecordTransferRequests = "//*[contains(text(), 'View student test attempt transfer requests')]";


	String lblHEader = ".//*[@id='content']/div[3]/h1";	

	
	
	public ViewStudentRecordsTransferRequestPage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	
	public void verifyRecordTransferRequest () throws InterruptedException {
		
		clickButton(btnDelivering);
		clickButton(btnRecordTransferRequests);
		verifyPageTitle(lblHEader,"Test Attempt Transfer Request");	
	}
}

