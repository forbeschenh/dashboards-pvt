package commonPages;

import org.openqa.selenium.WebDriver;

import basePageObjects.BasicPageObjects;

/**
Author: dparakrama
Created : Mar 20, 2018
 */
public class SchoolPinsPage extends BasicPageObjects{
	String btnPreparing = ".//*[@id='workflow-progress-preparing']";
	String btnSchoolPins = "//*[contains(text(), 'Manage school PINs')]";

	String lblHEader = ".//*[@id='content']/div[3]/h1";
    String btnSchoolPinsspan = ".//*[@id='content']/fieldset/div/div/h2";
	
    String txtSchoolPinsInfo = ".//*[@id='ctrl.form']/div[2]/fieldset/div[1]/p/p";
	
	public SchoolPinsPage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	
	public void verifySchoolPins () throws InterruptedException {
		
		clickButton(btnPreparing);
		clickButton(btnSchoolPins);
		verifyPageTitle(lblHEader,"Multifactor Authentication");	
		clickButton(btnSchoolPinsspan);
		getContent(txtSchoolPinsInfo, "The purpose of a School PIN");
		
	}
}

