package commonPages;

import org.openqa.selenium.WebDriver;

import basePageObjects.BasicPageObjects;

/**
Author: dparakrama
Created : Mar 20, 2018
 */
public class SsrAndIsrPage extends BasicPageObjects {
	String btnResults = ".//*[@id='workflow-progress-results']";
	String btnSSSR = "//*[contains(text(), 'Release School and Student Summary Report (SSSR)')]";
	
	String lblHEader = "//*[@id='content']/div[3]/div[1]/h1";
	String btnSearchBar = ".//*[@id='content']/div[3]/form/h3";
	
	String txtName = ".//*[@id='search-nameFilter']";
	String btnSearch = ".//*[@id='btnPerformSearch']";
	String txtResult = ".//*[@id='Grid_1']/table/tbody/tr[1]/td[3]/a";
	
    
	public SsrAndIsrPage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	
	public void verifySSSRandISR () throws InterruptedException {
		
		clickButton(btnResults);
		clickButton(btnSSSR);
		verifyPageTitle(lblHEader,"Reporting Release Management");	
		clickButton(btnSearchBar);
		enterText(txtName,"ESA");
		clickButton(btnSearch);
		getContent(txtResult,"ESA");
	}
}

