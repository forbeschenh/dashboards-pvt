package commonPages;

import org.openqa.selenium.WebDriver;

import basePageObjects.BasicPageObjects;

/**
Author: dparakrama
Created : Mar 20, 2018
 */
public class MonitorDevicesPage extends BasicPageObjects {
	String btnDelivering= "//*[@id=\"workflow-progress-delivering\"]";
	String btnMonitorDevices = "//*[contains(text(), 'Monitor devices')]";


	String lblHEader = "//*[@id='content']/div[3]/div[1]/h1";	
	String btnSearchBar = ".//*[@id='content']/div[3]/form/h3";
	
	String btnSchoolInput = "//*[@id=\"s2id_search-school\"]";
	String btnSchoolInput2 = "//*[@id=\"s2id_search-school\"]/ul/li/input";
	String btnDropItem = "//*[@id='select2-drop']/ul/li[2]/div";
	String btnSearch = ".//*[@id='btnPerformSearch']";
	String txtResult = ".//*[@id='Grid_1']/table/tbody/tr[2]/td[2]/a";
	
	
	
	public MonitorDevicesPage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	
	public void verifyMonitorDevice () throws InterruptedException {
		clickButton(btnDelivering);
		clickButton(btnMonitorDevices);
		verifyPageTitle(lblHEader,"Device List");	
		/*clickButton(btnSearchBar);
		clickButton(btnSchoolInput);
		enterText(btnSchoolInput2, "ESA");
		clickButton(btnDropItem);
		clickButton(btnSearch);*/
		getContent(txtResult, "ESA");
		
		
	}
}

