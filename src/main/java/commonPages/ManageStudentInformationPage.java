package commonPages;

import org.openqa.selenium.WebDriver;

import basePageObjects.BasicPageObjects;

/**
Author: dparakrama
Created : Mar 20, 2018
 */
public class ManageStudentInformationPage extends BasicPageObjects{
	String btnPreparing = ".//*[@id='workflow-progress-preparing']";
	String btnManageStudentInfo = "//*[@id=\"dashboard\"]/div[2]/div/div[4]/div[3]/div[2]/div/div/div/div/div/div[1]/div[2]/a[5]";

	String lblHEader = ".//*[@id='content']/div[3]/h1";
	String btnSearchBar = ".//*[@id='content']/div[6]/form/h3";
	String txtSchoolName = ".//*[@id='search-nameFilter']";
	String btnSearch =".//*[@id='btnPerformSearch']";
	String txtResult = "//*[@id='Grid_1']/table/tbody/tr[1]/td[3]/span/a[1]";
	                     

	public ManageStudentInformationPage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

public void verifyManageStudentInfo() throws InterruptedException {
		
		clickButton(btnPreparing);
		clickButton(btnManageStudentInfo);
		verifyPageTitle(lblHEader,"Students");	
		clickButton(btnSearchBar);
		enterText(txtSchoolName, "ESA");
		clickButton(btnSearch);
		//getContent(txtResult, "esa");
		
	}
}

