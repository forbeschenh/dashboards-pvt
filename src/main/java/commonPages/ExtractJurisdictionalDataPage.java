package commonPages;

import org.openqa.selenium.WebDriver;

import basePageObjects.BasicPageObjects;

/**
Author: dparakrama
Created : Mar 20, 2018
 */
public class ExtractJurisdictionalDataPage extends BasicPageObjects {

	String btnResults = ".//*[@id='workflow-progress-results']";
	String btnExtractJurisdictionalData = "//*[contains(text(), 'Extract jurisdictional data')]";
	
	String lblHEader = ".//*[@id='content']/div[3]/h1";
    
	
	public ExtractJurisdictionalDataPage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	public void verifyExtracts () throws InterruptedException{
		
		clickButton(btnResults);
		clickButton(btnExtractJurisdictionalData);
		verifyPageTitle(lblHEader,"Results & Reporting Dataset");
	}
	
}

