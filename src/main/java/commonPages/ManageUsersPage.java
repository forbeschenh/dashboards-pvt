package commonPages;

import org.openqa.selenium.WebDriver;

import basePageObjects.BasicPageObjects;

/**
Author: dparakrama
Created : Mar 20, 2018
 */
public class ManageUsersPage extends BasicPageObjects {
	String btnPreparing = ".//*[@id='workflow-progress-preparing']";
	//String btnManageUsers = "//a[contains(text(),'Manage users')]";


	String lblHEader = ".//*[@id='content']/div[3]/h1";	
	String btnSearchBar = ".//*[@id='content']/div[6]/form/h3";
	
	String txtSearchNameFilter = ".//*[@id='search-nameFilter']";
	String btnSearch = ".//*[@id='btnPerformSearch']";
	String txtResult = ".//*[@id='Grid_1']/table/tbody/tr[1]/td[2]/span[1]/a";
	
	
	
	public ManageUsersPage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
	
	public void verifyManageUser (String btnManageUsers) throws InterruptedException {		
		clickButton(btnPreparing);
		Thread.sleep(3000);
		clickButton(btnManageUsers);
		Thread.sleep(3000);
		//verifyPageTitle(lblHEader,"Users");	
		enterText(txtSearchNameFilter, "esa");
		clickButton(btnSearch);		
		//getContent(txtResult, "esa");
	}
	
	

}

