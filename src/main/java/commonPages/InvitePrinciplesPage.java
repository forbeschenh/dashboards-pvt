package commonPages;

import org.openqa.selenium.WebDriver;

import basePageObjects.BasicPageObjects;

/**
Author: dparakrama
Created : Mar 20, 2018
 */
public class InvitePrinciplesPage extends BasicPageObjects {

	String btnPreparing = ".//*[@id='workflow-progress-preparing']";
	String btnInvitePrinciple = "//*[contains(text(), 'Invite principal (single)')]";

	String lblHEader = ".//*[@id='content']/div[3]/h1";
	
	String drpSchool = ".//*[@id='s2id_OrgUnitId']/a/span";
	String txtSchool = ".//*[@id='select2-drop']/div/input";
	String btnSchoolSelection = ".//*[@id='select2-drop']/ul/li[2]/div";		
	String txtEmailAdress = ".//*[@id='EmailAddresses']";
	
	String txtEmailSubject = ".//*[@id='EmailSubject']";
	String btnSendEmail = ".//*[@id='content']/form/div[3]/div/span/button";
	
	public InvitePrinciplesPage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	public void verifyInviteSinglePrinciple (String Email) throws InterruptedException {
		
		clickButton(btnPreparing);
		clickButton(btnInvitePrinciple);	
		verifyPageTitle(lblHEader,"Invite Principals via secure link");	
		clickButton(drpSchool);
		clickButton(btnSchoolSelection);	
		enterText(txtEmailAdress, Email);
		clickButton(btnSendEmail);
	}
	
	
}

