package commonPages;

import org.openqa.selenium.WebDriver;

import basePageObjects.BasicPageObjects;

/**
Author: dparakrama
Created : Mar 20, 2018
 */
public class ManageSchoolInformationPage extends BasicPageObjects{

	String btnPreparing = ".//*[@id='workflow-progress-preparing']";
	String btnManageSchoolInfo = "//*[contains(text(), 'Manage school information')]";

	String lblHEader = ".//*[@id='content']/div[3]/h1";
	String btnSearchBar = ".//*[@id='content']/div[6]/form/h3";
	String txtSchoolName = ".//*[@id='search-nameFilter']";
	String btnSearch =".//*[@id='btnPerformSearch']";
	String txtResult = ".//*[@id='Grid_1']/table/tbody/tr[1]/td[3]/span/a[1]";
		

	
	
	public ManageSchoolInformationPage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	
	public void verifyManageSchoolInfo() throws InterruptedException {
		
		clickButton(btnPreparing);
		clickButton(btnManageSchoolInfo);
		verifyPageTitle(lblHEader,"Schools");	
		clickButton(btnSearchBar);
		enterText(txtSchoolName, "ESA");
		clickButton(btnSearch);
		//getContent(txtResult, "ESA");
		
	}
}

