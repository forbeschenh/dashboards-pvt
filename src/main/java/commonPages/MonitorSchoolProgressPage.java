package commonPages;

import org.openqa.selenium.WebDriver;

import basePageObjects.BasicPageObjects;

/**
Author: dparakrama
Created : Mar 20, 2018
 */
public class MonitorSchoolProgressPage extends BasicPageObjects{
	
	String btnDelivering= ".//*[@id='workflow-progress-delivering']";
	String btnManageUsers = "//*[contains(text(), 'Monitor school progress')]";


	String lblHEader = ".//*[@id='content']/div[3]/h1";	
	String btnSearchBar = ".//*[@id='content']/div[6]/div[1]/form/h3";
	String txtSchoolName = ".//*[@id='search-nameFilter']";
	
    String btnSearch = ".//*[@id='btnPerformSearch']";
    String txtResult = ".//*[@id='Grid_1']/table/tbody/tr[1]/td[1]/a";
    
	public MonitorSchoolProgressPage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	
	public void verifyMonitoringProgress() throws InterruptedException {
		
		clickButton(btnDelivering);
		clickButton(btnManageUsers);
		verifyPageTitle(lblHEader,"School Assessment Program Progress");	
		clickButton(btnSearchBar);
		enterText(txtSchoolName, "ESA");
		clickButton(btnSearch);
		getContent(txtResult, "ESA");
		
		
	}
}

