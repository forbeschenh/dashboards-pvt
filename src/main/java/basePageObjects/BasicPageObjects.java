package basePageObjects;
import java.io.IOException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;



public class BasicPageObjects {
	private WebDriver driver;

	public BasicPageObjects(WebDriver driver) {
		this.driver = driver;
	}

	/***
	 * Service Method for Clicking links
	 * 
	 * @throws InterruptedException
	 * **/
	public boolean clickLink(String link) throws InterruptedException {
		WebElement Mainlink = driver.findElement(By.xpath(link));
		if (Mainlink.isDisplayed()) {
			System.out.println("LINK TEXT IS CORRECT, LINK TEXT IS : "
					+ Mainlink.getText());
			Mainlink.click();
			Thread.sleep(1000);
			return true;
		} else {
			System.out
					.println("ELEMENT IS NOT VISIBLE");
			throw new InterruptedException();
		}
	}

	/***
	 * Service Method for Text boxes
	 * 
	 * @throws InterruptedException
	 * **/

	public void enterText(String TextboxXpath, String Text)
			throws InterruptedException {
		
		if (waitUntillxpath(TextboxXpath)) 
		{WebElement textbox = driver.findElement(By.xpath(TextboxXpath));
		if (textbox.isDisplayed())
			textbox.clear();
		textbox.sendKeys(Text);} 
		else {System.out.println("ELEMENT NOT FOUND, XPATH:  " + TextboxXpath);	}
		Thread.sleep(1000);
	}

	/***
	 * Service Method for Buttons
	 * 
	 * @throws InterruptedException
	 * **/

	public void clickButton(String ButtonXpath) throws InterruptedException {
		
		if (waitUntillxpath(ButtonXpath)) 
		{WebElement button = driver.findElement(By.xpath(ButtonXpath));
		if (button.isDisplayed())
			button.click();} 
		else {System.out.println("ELEMENT NOT FOUND, XPATH:  " + ButtonXpath);	}
		Thread.sleep(1000);
	}

	/***
	 * Service Methods for Buttons
	 * 
	 * @throws InterruptedException
	 * **/

	public void clickButtonbyCss(String ButtonCSS) throws InterruptedException {
		if (waitUntillCSS(ButtonCSS)) 
		{WebElement button = driver.findElement(By.cssSelector(ButtonCSS));
		if (button.isDisplayed())
			button.click();}
		else {System.out.println("ELEMENT NOT FOUND, CSS:  " + ButtonCSS);	}
		Thread.sleep(1000);
	}

	/***
	 * Service Method for Verify Page Headers
	 * 
	 * @throws InterruptedException
	 * **/
	public boolean verifyPageTitle(String HeaderXapath, String Title)
			throws InterruptedException {
		WebElement PageHeader = driver.findElement(By.xpath(HeaderXapath));
		String HeaderContains = PageHeader.getText();
		if (PageHeader.isDisplayed() && HeaderContains.contains(Title)) {
			System.out.println("LINK TEXT IS CORRECT, LINK TEXT IS :   "
					+ PageHeader.getText());
			Thread.sleep(1000);
			return true;
		} else {
			System.out
					.println("PAGE ELEMENT NOT VISIBLE");
			throw new InterruptedException();
		}
	}

	/***
	 * Service Method for DropDowns
	 * 
	 * @throws InterruptedException
	 * **/

	public void clickDropDown(String DropDownXpath, String DropDownValue,
			String Value1, String Value2, String Value3, String Value4)
			throws InterruptedException {
		WebElement dropDown = driver.findElement(By.xpath(DropDownXpath));
		if (dropDown.isDisplayed()) {
			dropDown.click();
			Thread.sleep(1000);

			if (DropDownValue.equals(Value1)) {
				driver.findElement(
						By.xpath("//*[@id='select2-drop']/ul/li[1]/div"))
						.click();
			}

			else if (DropDownValue.equals(Value2)) {
				driver.findElement(
						By.xpath("//*[@id='select2-drop']/ul/li[2]/div"))
						.click();
			}

			else if (DropDownValue.equals(Value3)) {
				driver.findElement(
						By.xpath("//*[@id='select2-drop']/ul/li[3]/div"))
						.click();
			}

			else if (DropDownValue.equals(Value4)) {
				driver.findElement(
						By.xpath("//*[@id='select2-drop']/ul/li[4]/div"))
						.click();
			}

			else {

				System.out.println("ELEMENT IS NOT DISPLAYED");
				throw new InterruptedException();

			}

		}

	}

	/***
	 * Service Method for DropDowns
	 * 
	 * @throws InterruptedException
	 * **/

	public void clickDropDownTypeTwo(String DropDownXpath,
			String DropDownValue, String Value1, String Value2, String Value3,
			String Value4, String Value5) throws InterruptedException {
		WebElement dropDown = driver.findElement(By.xpath(DropDownXpath));
		if (dropDown.isDisplayed()) {
			dropDown.click();
			Thread.sleep(1000);

			if (DropDownValue.equals(Value1)) {
				driver.findElement(
						By.xpath("//*[@id='select2-drop']/ul/li/ul/li[1]/div"))
						.click();
			}

			else if (DropDownValue.equals(Value2)) {
				driver.findElement(
						By.xpath("//*[@id='select2-drop']/ul/li/ul/li[2]/div"))
						.click();
			}

			else if (DropDownValue.equals(Value3)) {
				driver.findElement(
						By.xpath("//*[@id='select2-drop']/ul/li/ul/li[3]/div"))
						.click();
			}

			else if (DropDownValue.equals(Value4)) {
				driver.findElement(
						By.xpath("//*[@id='select2-drop']/ul/li/ul/li[4]/div"))
						.click();
			} else if (DropDownValue.equals(Value5)) {
				driver.findElement(
						By.xpath("//*[@id='select2-drop']/ul/li/ul/li[5]/div"))
						.click();
			} else {

				System.out.println("ELEMENT IS NOT DISPLAYED");
				throw new InterruptedException();

			}

		}

	}

	/***
	 * Service Method to verify the contains of a label ( Ex:- Page Header)
	 * 
	 * @throws InterruptedException
	 * **/

	public void getContent(String Labelxpath, String Text) throws InterruptedException {
		WebElement label = driver.findElement(By.xpath(Labelxpath));
		String content = label.getText();
		if (content.contains(Text)) {
			System.out.println("LABEL IS CORRECT : " + content);
		} else {
			System.out.println("LABEL IS INCORRECT");
			throw new InterruptedException();
		}
		Thread.sleep(1000);
	}

	/***
	 * Service Method for tables and finding values in a table
	 * 
	 * @throws InterruptedException
	 * **/

	public void findTableElement(String tableXpath, String searchStringValue,
			String firstXpathBeforRowNumber, String LastXpathBeforeRowNumber) throws InterruptedException {

		WebElement table = driver.findElement(By.xpath(tableXpath));
		List<WebElement> rows_table = table.findElements(By.tagName("tr"));
		int rows_count = rows_table.size();

		for (int row = 0; row < rows_count; row++) {
			List<WebElement> Columns_row = rows_table.get(row).findElements(
					By.tagName("td"));
			int columns_count = Columns_row.size();

			for (int column = 0; column < columns_count; column++) {
				String celltext = Columns_row.get(column).getText();

				if (celltext.equals(searchStringValue)) {
					driver.findElement(
							By.xpath(firstXpathBeforRowNumber + row
									+ LastXpathBeforeRowNumber)).click();
					Thread.sleep(1000);
				}

			}
		}

	}


	/***
	 * Service Method for verifying an element is present
	 * 
	 * @throws InterruptedException
	 * 
	 * 
	 * **/

	public void isElementPresent(String xpath, String text)
			throws InterruptedException {

		driver.findElement(By.xpath(xpath));

		String word = driver.findElement(By.xpath(xpath)).getText();
		if (word.contains(text)) {
			System.out.println("ELEMENT FOUND, ELEMENT TEXT:  "
					+ driver.findElement(By.xpath(xpath)).getText());

		} else {
			throw new InterruptedException("ELEMENT CAN NOT BE FOUND: XPATH" + xpath + "ELEMENT LABEL" + text);

		}
	}
	
	
public boolean waitUntillxpath (String xpath) throws InterruptedException {
	int x=0;
	for(int i=0; i < 10000; i=i+500) {
		x = driver.findElements(By.xpath(xpath)).size();
		if (x==0) {} else {return true;}
		Thread.sleep(500);
	}
	if (x==0) {return false;} 
	else {return true;}
	
	}


	
public boolean waitUntillCSS (String CSS) throws InterruptedException {
	int x=0;
	for(int i=0; i < 10000; i=i+500) {
		x = driver.findElements(By.cssSelector(CSS)).size();
		if (x==0) {} else {return true;}
		Thread.sleep(500);
	}
	if (x==0) {return false;} 
	else {return true;}
	
	}



public boolean isChecked(String xpath) throws InterruptedException {

	String CheckboxClassName = driver.findElement(By.xpath(xpath)).getAttribute("class");

	if (CheckboxClassName.equals("ui-check-radio")) {
		System.out.println("ELEMENT IS NOT SELECTED -> xpath:  " + xpath);
		return false;
	}

	else if (CheckboxClassName.equals("ui-check-radio ui-check-radio-checked")) {
		System.out.println("ELEMENT IS SELECTED -> xpath:  " + xpath);
		return true;
	}
	
	else
	{return false;}

} 


}

	

