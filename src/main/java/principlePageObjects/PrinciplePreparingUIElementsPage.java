package principlePageObjects;

import org.openqa.selenium.WebDriver;

import basePageObjects.BasicPageObjects;

/**
Author: dparakrama
Created : Mar 20, 2018
 */
public class PrinciplePreparingUIElementsPage extends BasicPageObjects{
	
	String btnPreparings = ".//*[@id='workflow-progress-preparing']";
	String ChecklistElement1 = ".//*[@id='dashboard-panel-trt-checklist']/div[2]/div/div/div/div/p[1]";
	String ChecklistElement2 = ".//*[@id='dashboard-panel-trt-checklist']/div[2]/div/div/div/div/p[2]";	
	String ChecklistElement3 = ".//*[@id='dashboard-panel-trt-checklist']/div[2]/div/div/div/div/p[3]";
	String ChecklistElement4 = ".//*[@id='dashboard-panel-trt-checklist']/div[2]/div/div/div/div/p[4]";	
	String ChecklistElement5 = ".//*[@id='dashboard-panel-trt-checklist']/div[2]/div/div/div/div/p[5]";	
	String ChecklistElement6 = ".//*[@id='dashboard-panel-trt-checklist']/div[2]/div/div/div/div/p[6]";	
	String ChecklistElement7 = ".//*[@id='dashboard-panel-trt-checklist']/div[2]/div/div/div/div/p[7]";	
	String ChecklistElement8 = ".//*[@id='dashboard-panel-trt-checklist']/div[2]/div/div/div/div/p[8]";	
	String ChecklistElement9 = ".//*[@id='dashboard-panel-trt-checklist']/div[2]/div/div/div/div/p[9]";	
	String ChecklistElement10 = ".//*[@id='dashboard-panel-trt-checklist']/div[2]/div/div/div/div/p[10]";	
	String ChecklistElement11 = ".//*[@id='dashboard-panel-trt-checklist']/div[2]/div/div/div/div/p[11]";
	
	String lnkStudentParticpationStatus = ".//*[@id='dashboard-panel-student-participation-chart']/div[1]/h3";
	String lkSP1 = ".//*[@id='student-participation-chart-pie']/div/div/div/div/div[1]/span/a";
	String lkSP2 = ".//*[@id='student-participation-chart-pie']/div/div/div/div/div[2]/span/a";
	String lkSP3 = ".//*[@id='student-participation-chart-pie']/div/div/div/div/div[3]/span/a";
	String lkSP4 = ".//*[@id='student-participation-chart-pie']/div/div/div/div/div[4]/span/a";
	String lkSP5 = ".//*[@id='student-participation-chart-pie']/div/div/div/div/div[5]/span/a";
	
	String lnkManageStudents = ".//*[@id='dashboard-panel-manage-students-registration']/div[1]";
	String lnkMS1 = ".//*[@id='dashboard-panel-manage-students-registration']/div[2]/a[1]";
	String lnkMS2 = ".//*[@id='dashboard-panel-manage-students-registration']/div[2]/a[2]";
	String lnkMS3 = ".//*[@id='dashboard-panel-manage-students-registration']/div[2]/a[3]";
	
	String lnkManageSchoolBasedUsers = ".//*[@id='dashboard-panel-manage-students']/div[1]/h3";
	String lnkMSc1 = "//a[contains(text(),'Invite NAPLAN Coordinators')]";
	String lnkMSc2 = "//a[contains(text(),'Manage NAPLAN Coordinator')]";
	String lnkMSc3 = "//a[contains(text(),'Invite School Technical Support Officer')]";
	String lnkMSc4 = "//a[contains(text(),'Manage School Technical Support Officer')]";
	
	
	String lnkTestSessionSchedule  = ".//*[@id='dashboard-panel-manage-session-timetable']/div[1]/h3";
	String lnkTSS1 = ".//*[@id='dashboard-panel-manage-session-timetable']/div[2]/a[1]";
	String lnkTSS2 = ".//*[@id='dashboard-panel-manage-session-timetable']/div[2]/a[2]";
	
	String lnkTechnicalReadiness = ".//*[@id='dashboard-panel-technical-readiness-testing']/div[1]/h3";
	String lnkTR1 = ".//*[@id='dashboard-panel-technical-readiness-testing']/div[2]/a[1]";
	String lnkTR2 = ".//*[@id='dashboard-panel-technical-readiness-testing']/div[2]/a[2]";
	String lnkTR3 = ".//*[@id='dashboard-panel-technical-readiness-testing']/div[2]/a[3]";
	
	
	
	String lnkReports = ".//*[@id='dashboard-panel-preparing-reports']/div[1]/h3";
	String lnkR1 = ".//*[@id='dashboard-panel-preparing-reports']/div[2]/a[1]";
	String lnkR2 = ".//*[@id='dashboard-panel-preparing-reports']/div[2]/a[2]";
	String lnkR3 = ".//*[@id='dashboard-panel-preparing-reports']/div[2]/a[3]";
	String lnkR4 = ".//*[@id='dashboard-panel-preparing-reports']/div[2]/a[4]";
	String lnkR5 = ".//*[@id='dashboard-panel-preparing-reports']/div[2]/a[5]";
	

	public PrinciplePreparingUIElementsPage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
	public void verifyChecklistItems() throws InterruptedException {
		clickButton(btnPreparings);
	    Thread.sleep(3000);
		isElementPresent(ChecklistElement1,
				"Handbook for Principals and Test Administration Handbook have been received and distributed.");
		isElementPresent(ChecklistElement2,
				"Parent information packs have been distributed.");
		isElementPresent(ChecklistElement3,
				"School technical readiness checks have been completed and delivery mode approved � online, low bandwidth school server, offline, paper.");
		isElementPresent(ChecklistElement4,
				"NAPLAN Coordinators are identified, trained and set up within the assessment platform.");
		isElementPresent(ChecklistElement5,
				"Need for withdrawal and exemption requests have been reviewed. If required request form completed and submitted to the TAA.");
		isElementPresent(ChecklistElement6,
				"Need for disability adjustment requests have been reviewed. If required request form completed and submitted to the TAA.");
		isElementPresent(ChecklistElement7,
				"Need for alternative format test material has been reviewed. If required request form completed and submitted to the TAA.");
		isElementPresent(ChecklistElement8,
				"Test administration staff are identified and trained within the assessment platform.");
		isElementPresent(ChecklistElement9,
				"Participating students' information within the ADS is complete and up-to-date.");
		isElementPresent(ChecklistElement10,
				"School and student device checks have been completed.");
		isElementPresent(ChecklistElement11,
				"Initial test session timetable has been completed and distributed to staff.");
	}
	
	
	public void verifyStudentParticpationStatusGraph () throws InterruptedException {
		
		/*isElementPresent(lnkStudentParticpationStatus,
				"Student Participation Status");*/
		isElementPresent(lkSP1,
				"Participating");
		isElementPresent(lkSP2,
				"Exempt");
		isElementPresent(lkSP3,
				"Withdrawn");
		isElementPresent(lkSP4,
				"Absent");
		isElementPresent(lkSP5,
				"No longer enrolled");
		
		
	}
	
	
	public void verifyPageLinksSRMActive () throws InterruptedException {

		
		
		isElementPresent(lnkManageSchoolBasedUsers,
				"Manage School-based Users");
		isElementPresent(lnkMSc1,
				"Invite NAPLAN Coordinators");
		isElementPresent(lnkMSc2,
				"Manage NAPLAN Coordinator");
		isElementPresent(lnkMSc3,
				"Invite School Technical Support Officer");
		isElementPresent(lnkMSc4,
				"Manage School Technical Support Officer");
		
		
		
		isElementPresent(lnkTestSessionSchedule,
				"Test Session Schedule");
		isElementPresent(lnkTSS1,
				"Create test session schedule");
		isElementPresent(lnkTSS2,
				"Manage test session schedule");
		
		
		
		isElementPresent(lnkTechnicalReadiness,
				"Technical Readiness");
		isElementPresent(lnkTR1,
				"Manage school network capacity assessment");
		isElementPresent(lnkTR2,
				"Device check");
		isElementPresent(lnkTR3,
				"Monitor device assessment");
		
	}
	
	
	public void verifyPageLinksSRMInactive () throws InterruptedException {
		
		isElementPresent(lnkMS1,
				"Create student");
		isElementPresent(lnkMS2,
				"Manage student information");
		isElementPresent(lnkMS3,
				"Manage student participation and disability adjustments");
		
		
	isElementPresent(lnkManageSchoolBasedUsers,
				"Manage School-based Users");
	isElementPresent(lnkMSc1,
				"Invite NAPLAN Coordinators");
		isElementPresent(lnkMSc2,
				"Manage NAPLAN Coordinator");
		isElementPresent(lnkMSc3,
				"Invite School Technical Support Officer");
		isElementPresent(lnkMSc4,
				"Manage School Technical Support Officer");
		
		
		
		isElementPresent(lnkTestSessionSchedule,
				"Test Session Schedule");
		isElementPresent(lnkTSS1,
				"Create test session schedule");
		isElementPresent(lnkTSS2,
				"Manage test session schedule");
		
		
		
		isElementPresent(lnkTechnicalReadiness,
				"Technical Readiness");
		isElementPresent(lnkTR1,
				"Manage school network capacity assessment");
		isElementPresent(lnkTR2,
				"Device check");
		isElementPresent(lnkTR3,
				"Monitor device assessment");
		
		isElementPresent(lnkReports,
				"Reports");
		isElementPresent(lnkR1,
				"Technical Readiness");
		isElementPresent(lnkR2,
				"Student Disability Adjustments");
		isElementPresent(lnkR3,
				"Exempted and Withdrawn Students");
		isElementPresent(lnkR4,
				"Participating Student");
		isElementPresent(lnkR5,
				"Test Administration");
		
	
		


	}
}

