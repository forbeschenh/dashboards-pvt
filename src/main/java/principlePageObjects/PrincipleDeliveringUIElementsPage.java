package principlePageObjects;

import org.openqa.selenium.WebDriver;

import basePageObjects.BasicPageObjects;

/**
Author: dparakrama
Created : Mar 20, 2018
 */
public class PrincipleDeliveringUIElementsPage extends BasicPageObjects {
	String btnDelivering= ".//*[@id='workflow-progress-delivering']";
	String ChecklistElement1 = ".//*[@id='dashboard-panel-registration-checklist']/div[2]/div/div/div/div/p[1]/a/span[2]";
	String ChecklistElement2 = ".//*[@id='dashboard-panel-registration-checklist']/div[2]/div/div/div/div/p[2]/a/span[2]";	
	String ChecklistElement3 = ".//*[@id='dashboard-panel-registration-checklist']/div[2]/div/div/div/div/p[3]/a/span[2]";
	String ChecklistElement4 = ".//*[@id='dashboard-panel-registration-checklist']/div[2]/div/div/div/div/p[4]/a/span[2]";	
	String ChecklistElement5 = ".//*[@id='dashboard-panel-registration-checklist']/div[2]/div/div/div/div/p[5]/a/span[2]";	
	
	
	String lnkTestAttemptSTatus = ".//*[@id='dashboard-panel-student-participation-chart']/div[1]/h3";
	String lkTS1 = ".//*[@id='test-attempt-status-chart-pie']/div/div/div/div/div[1]/span/a";
	String lkTS2 = ".//*[@id='test-attempt-status-chart-pie']/div/div/div/div/div[2]/span/a";
	String lkTS3 = ".//*[@id='test-attempt-status-chart-pie']/div/div/div/div/div[3]/span/a";
	String lkTS4 = ".//*[@id='test-attempt-status-chart-pie']/div/div/div/div/div[4]/span/a";
	String lkTS5 = ".//*[@id='test-attempt-status-chart-pie']/div/div/div/div/div[5]/span/a";
	String lkTS6 = ".//*[@id='test-attempt-status-chart-pie']/div/div/div/div/div[6]/span/a";
	
	String lnkStudentParticpationStatus = ".//*[@id='dashboard-panel-student-participation-chart']/div[1]/h3";
	String lnkSP1 = ".//*[@id='student-participation-chart-pie-2']/div/div/div/div/div[1]/span/a";
	String lnkSP2 = ".//*[@id='student-participation-chart-pie-2']/div/div/div/div/div[2]/span/a";
	String lnkSP3 = ".//*[@id='student-participation-chart-pie-2']/div/div/div/div/div[3]/span/a";
	String lnkSP4 = ".//*[@id='student-participation-chart-pie-2']/div/div/div/div/div[4]/span/a";
	String lnkSP5 = ".//*[@id='student-participation-chart-pie-2']/div/div/div/div/div[5]/span/a";
	
	String lnkTestSessionStatusSummary = ".//*[@id='dashboard-panel-test-session-status-summary-chart']/div[1]/h3";
	String lnkTSS1 = ".//*[@id='test-session-status-summary-chart-pie']/div/div/div/div/div[1]/span/a";
	String lnkTSS2 = ".//*[@id='test-session-status-summary-chart-pie']/div/div/div/div/div[2]/span/a";
	String lnkTSS3 = ".//*[@id='test-session-status-summary-chart-pie']/div/div/div/div/div[3]/span/a";
	String lnkTSS4 = ".//*[@id='test-session-status-summary-chart-pie']/div/div/div/div/div[4]/span/a";
	
	
	String lnkTestSessionPreparation = ".//*[@id='dashboard-panel-manage-test-administrators']/div[1]/h3";
	String lnkUnused = ".//*[@id='dashboard-panel-manage-test-administrators']/div[2]/p";
	String lnkTSP1 = ".//*[@id='dashboard-panel-manage-test-administrators']/div[3]/a[1]";
	String lnkTSP2 = ".//*[@id='dashboard-panel-manage-test-administrators']/div[3]/a[2]";
	String lnkTSP3 = ".//*[@id='dashboard-panel-manage-test-administrators']/div[3]/a[3]";
	String lnkTSP4 = ".//*[@id='dashboard-panel-manage-test-administrators']/div[3]/a[4]";
	String lnkTSP5 = ".//*[@id='dashboard-panel-manage-test-administrators']/div[3]/a[5]";
	
	
	String lnkManageTestSessions = ".//*[@id='dashboard-panel-manage-test-sessions']/div[1]/h3";
	String lnkMT1= ".//*[@id='dashboard-panel-manage-test-sessions']/div[2]/a[1]";
	String lnkMT2= ".//*[@id='dashboard-panel-manage-test-sessions']/div[2]/a[2]";
	
	String lnkReports = ".//*[@id='dashboard-panel-delivering-reports']/div[1]/h3";
	String lnkR1 = ".//*[@id='dashboard-panel-delivering-reports']/div[2]/a[1]";
	String lnkR2 = ".//*[@id='dashboard-panel-delivering-reports']/div[2]/a[2]";
	String lnkR3 = ".//*[@id='dashboard-panel-delivering-reports']/div[2]/a[3]";
	String lnkR4 = ".//*[@id='dashboard-panel-delivering-reports']/div[2]/a[4]";
	String lnkR5 = ".//*[@id='dashboard-panel-delivering-reports']/div[2]/a[5]";
	String lnkR6 = ".//*[@id='dashboard-panel-delivering-reports']/div[2]/a[6]";
	String lnkR7 = ".//*[@id='dashboard-panel-delivering-reports']/div[2]/a[7]";
	
	
	
	
	public PrincipleDeliveringUIElementsPage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	
	
	public void verifyChecklistItems () throws InterruptedException {
		
		clickButton(btnDelivering);
		isElementPresent(ChecklistElement1,
				"The test administrator test session packs have been prepared and distributed.");
		isElementPresent(ChecklistElement2,
				"The assigned test rooms have been prepared and are ready for use.");
		isElementPresent(ChecklistElement3,
				"All of the planned test sessions have been completed and closed.");
		isElementPresent(ChecklistElement4,
				"All of the student test attempts have been submitted and finalised.");
		isElementPresent(ChecklistElement5,
				"All of the alternative format test materials have been returned to the TAA.");
		
	}
	
	public void verifyGraphs () throws InterruptedException {
		/*isElementPresent(lnkTestAttemptSTatus,
				"Test Attempt Status");*/
		isElementPresent(lkTS1,
				"Submitted");
		isElementPresent(lkTS2,
				"Open");
		isElementPresent(lkTS3,
				"Abandoned");
		isElementPresent(lkTS4,
				"Postponed");
		isElementPresent(lkTS5,
				"Refused");
		isElementPresent(lkTS6,
				"Other");
		
		/*isElementPresent(lnkStudentParticpationStatus,
				"Student Participation Status");*/
		isElementPresent(lnkSP1,
				"Participating");
		isElementPresent(lnkSP2,
				"Exempt");
		isElementPresent(lnkSP3,
				"Withdrawn");
		isElementPresent(lnkSP4,
				"Absent");
		isElementPresent(lnkSP5,
				"No longer enrolled");
		
		
		/*isElementPresent(lnkTestSessionStatusSummary,
				"Student Participation Status");
		
		isElementPresent(lnkTSS1,
				"Open");
		isElementPresent(lnkTSS2,
				"Started");
		isElementPresent(lnkTSS3,
				"Finished");
		isElementPresent(lnkTSS4,
				"Cancelled");*/
	}
	
	
	public void verifyFunctionalPageLinks () throws InterruptedException {
		/*isElementPresent(lnkTestSessionPreparation,
				"Test Session Preparation");*/
		isElementPresent(lnkUnused,
				"unused session logins");
		
		isElementPresent(lnkTSP1,
				"Create TA session login");
		isElementPresent(lnkTSP2,
				"Manage TA session login");
		isElementPresent(lnkTSP3,
				"Print TA Session Slip");
		isElementPresent(lnkTSP4,
				"Print Student Session Slip");
		isElementPresent(lnkTSP5,
				"Test Session Status Report");
		
	
		/*isElementPresent(lnkManageTestSessions,
				"Manage Test Sessions");
		*/
		isElementPresent(lnkMT1,
				"Manage test sessions");
		isElementPresent(lnkMT2,
				"Manage test attempts");
	
		
		/*isElementPresent(lnkReports,
				"Reports");*/
		
		isElementPresent(lnkR1,
				"Open Test Session");
		isElementPresent(lnkR2,
				"Test Disruption");
		isElementPresent(lnkR3,
				"Test Session Finalisation");
		isElementPresent(lnkR4,
				"Multi school student and student information QA");
		isElementPresent(lnkR5,
				"Hosted and Visiting Students");
		isElementPresent(lnkR6,
				"Excluded Students");
		isElementPresent(lnkR7,
				"Student Participation Summary");
		
	
	}
	
}

