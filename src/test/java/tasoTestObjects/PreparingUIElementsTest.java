package tasoTestObjects;

import java.io.IOException;
import org.openqa.selenium.By;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import tasoPageObjects.TAASOAllPreparingUIElementsPage;
import com.relevantcodes.extentreports.LogStatus;
import baseTestObjects.BasicTestObjects;
import baseTestObjects.ProdTestBase;


public class PreparingUIElementsTest extends BasicTestObjects {

	@Parameters({ "appURL", "TAASOUserName", "TAASOPassword" })
	@Test
	public void verifyTAASOAllPreparingUIElementsPage(String url,
			String UserName, String Password) throws InterruptedException, IOException {
		browserSetup(url);
		ProdTestBase PordTestBase = new ProdTestBase(driver);
		PordTestBase.doLogin(UserName, Password);
		driver.findElement(By.xpath(".//*[@id='workflow-progress-preparing']")).click();
		Thread.sleep(4000);
		TAASOAllPreparingUIElementsPage TAASOAllPreparingUIElementsPage = new TAASOAllPreparingUIElementsPage(driver);
		TAASOAllPreparingUIElementsPage.verifyChecklistItems();
		TAASOAllPreparingUIElementsPage.verifyManageUsersLinksSRMInactive();
		TAASOAllPreparingUIElementsPage.verifyManageJurisdictionInformation();
		TAASOAllPreparingUIElementsPage.verifyReports();
		TAASOAllPreparingUIElementsPage.verifySchoolTechnicalReadinessGraph();
		TAASOAllPreparingUIElementsPage.verifySchoolDeliveryModeGraph();
		TAASOAllPreparingUIElementsPage.verifySchoolPreparationStatusGraph();
		TAASOAllPreparingUIElementsPage.verifyStudentParticipationStatusGraph();
		test.log(LogStatus.PASS, "VERIFIED ALL ELEMENTS IN THE PREPARING PAGE");
		closeSession();
}
}