package tasoTestObjects;

import java.io.IOException;

import org.openqa.selenium.By;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import basePageObjects.BasicPageObjects;
import baseTestObjects.BasicTestObjects;
import baseTestObjects.ProdTestBase;
import commonPages.CreateStudentPage;
import commonPages.ExcludeStudentRecordPage;
import commonPages.ExtractJurisdictionalDataPage;
import commonPages.InviteNaplanCoordinatorPage;
import commonPages.InvitePrinciplesPage;
import commonPages.ManageSchoolInformationPage;
import commonPages.ManageStudentInformationPage;
import commonPages.ManageStudentParticipationPage;
import commonPages.ManageUsersPage;
import commonPages.MonitorDevicesPage;
import commonPages.MonitorSchoolProgressPage;
import commonPages.SchoolPinsPage;
import commonPages.SsrAndIsrPage;
import commonPages.StudentRegistration;
import commonPages.ViewStudentRecordsTransferRequestPage;
import tasoPageObjects.TAASOAllDeliveringUIElementsPage;
import tasoPageObjects.TAASOAllPreparingUIElementsPage;
import tasoPageObjects.TAASOAllResultsUIElementsPage;

public class TASOTestsSRMActive extends BasicTestObjects {

	@Parameters({ "appURL", "SRMActive_TAASOUserName", "SRMActive_TAASOPassword", "Email" })
	@Test
	public void verifyPageNavigationAsTAASO(String url,
			String UserName, String Password, String Email) throws InterruptedException, IOException {
		browserSetup(url);
		ProdTestBase PordTestBase = new ProdTestBase(driver);
		PordTestBase.doLogin(UserName, Password);
		
		
    	InvitePrinciplesPage InvitePrinciplesPage = new InvitePrinciplesPage(driver);
		InvitePrinciplesPage.verifyInviteSinglePrinciple(Email);
		test.log(LogStatus.PASS, "VERIFIED SENDING AN EMAIL TO A SINGLE PRINCIPALS");
		
		driver.findElement(By.xpath(".//*[@id='main']/div/a")).click();
		Thread.sleep(5000);
		InviteNaplanCoordinatorPage InviteNaplanCoordinatorPage = new InviteNaplanCoordinatorPage(driver);
		InviteNaplanCoordinatorPage.verifyInviteNaplanCoordinator(Email);
		test.log(LogStatus.PASS, "VERIFIED SENDING AN EMAIL TO A NAPLAN COORDINATOR");
		
		
		driver.findElement(By.xpath(".//*[@id='main']/div/a")).click();
		Thread.sleep(5000);
		ManageUsersPage ManageUsersPage = new ManageUsersPage(driver);
		ManageUsersPage.verifyManageUser("//a[contains(@class,\"list-group-item element-sector-administrator-hidden element-taa-administrator-hidden element-main-role_super-admin-hidden element-main-role_super-super-admin-hidden\")]");
		test.log(LogStatus.PASS, "PERFORMED A USER SEARCH BY NAME");
		
		
		
		driver.findElement(By.xpath(".//*[@id='main']/div/a")).click();
		Thread.sleep(5000);
		ManageSchoolInformationPage ManageSchoolInformationPage = new ManageSchoolInformationPage(driver);
		ManageSchoolInformationPage.verifyManageSchoolInfo();
		test.log(LogStatus.PASS, "PERFORMED A SEARCH BY SCHOOL NAME");
		

		driver.findElement(By.xpath(".//*[@id='main']/div/a")).click();
		Thread.sleep(5000);
		SchoolPinsPage SchoolPinsPage= new SchoolPinsPage(driver);
		SchoolPinsPage.verifySchoolPins();
		test.log(LogStatus.PASS, "VERIFIED SCHOOL PIN PAGE");
		driver.findElement(By.xpath(".//*[@id='main']/div/a")).click();
		Thread.sleep(1000);
		
		driver.findElement(By.xpath(".//*[@id='workflow-progress-preparing']")).click();
		Thread.sleep(4000);
		TAASOAllPreparingUIElementsPage TAASOAllPreparingUIElementsPage = new TAASOAllPreparingUIElementsPage(driver);
		TAASOAllPreparingUIElementsPage.verifyChecklistItems();
		TAASOAllPreparingUIElementsPage.verifyManageUsersLinksSRMActive();
		TAASOAllPreparingUIElementsPage.verifyManageJurisdictionInformation();
		TAASOAllPreparingUIElementsPage.verifyReports();
		TAASOAllPreparingUIElementsPage.verifySchoolTechnicalReadinessGraph();
		TAASOAllPreparingUIElementsPage.verifySchoolDeliveryModeGraph();
		TAASOAllPreparingUIElementsPage.verifySchoolPreparationStatusGraph();
		TAASOAllPreparingUIElementsPage.verifyStudentParticipationStatusGraph();
		test.log(LogStatus.PASS, "VERIFIED ALL ELEMENTS IN THE PREPARING PAGE");
		
		
		StudentRegistration StudentRegistration = new StudentRegistration(driver);
		StudentRegistration.verifyStudentRegistrationLink();
		test.log(LogStatus.PASS, "VERIFIED MANAGE USER PAGE LOADING");
		
		closeSession();
}



	
/*End of Sub*/}
