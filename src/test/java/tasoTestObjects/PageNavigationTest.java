package tasoTestObjects;

import java.io.IOException;
import org.openqa.selenium.By;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import com.relevantcodes.extentreports.LogStatus;
import commonPages.CreateStudentPage;
import commonPages.ExcludeStudentRecordPage;
import commonPages.ExtractJurisdictionalDataPage;
import commonPages.InviteNaplanCoordinatorPage;
import commonPages.InvitePrinciplesPage;
import commonPages.ManageSchoolInformationPage;
import commonPages.ManageStudentParticipationPage;
import commonPages.MonitorDevicesPage;
import commonPages.MonitorSchoolProgressPage;
import commonPages.SchoolPinsPage;
import commonPages.SsrAndIsrPage;
import commonPages.ViewStudentRecordsTransferRequestPage;
import baseTestObjects.BasicTestObjects;
import baseTestObjects.ProdTestBase;

/**
Author: dparakrama
Created : Mar 20, 2018
 */
public class PageNavigationTest extends BasicTestObjects {
	@Parameters({ "appURL", "TAASOUserName", "TAASOPassword", "Email"})
	@Test
	public void verifyPageNavigationAsTAA(String url,
			String UserName, String Password, String Email) throws InterruptedException, IOException {
		browserSetup(url);
		ProdTestBase PordTestBase = new ProdTestBase(driver);
		PordTestBase.doLogin(UserName, Password);
    	InvitePrinciplesPage InvitePrinciplesPage = new InvitePrinciplesPage(driver);
		InvitePrinciplesPage.verifyInviteSinglePrinciple(Email);
		test.log(LogStatus.PASS, "VERIFIED SENDING AN EMAIL TO A SINGLE PRINCIPALS");
		
		
		driver.findElement(By.xpath(".//*[@id='main']/div/a")).click();
		Thread.sleep(5000);
		CreateStudentPage CreateStudnetPage = new CreateStudentPage(driver);
		CreateStudnetPage.verifyCreateStudnetPage();
		test.log(LogStatus.PASS, "VERIFIED CREATE STUDENT PAGE LOADING");
		
		driver.findElement(By.xpath(".//*[@id='main']/div/a")).click();
		Thread.sleep(5000);
		ManageStudentParticipationPage ManageStudentParticipationPage = new ManageStudentParticipationPage(driver);
		ManageStudentParticipationPage.verifyManageStudentP();
		test.log(LogStatus.PASS, "PERFORMED A SEARCH BY SCHOOL NAME AND GROUP LEVEL");
		
		driver.findElement(By.xpath(".//*[@id='main']/div/a")).click();
		Thread.sleep(5000);
		InviteNaplanCoordinatorPage InviteNaplanCoordinatorPage = new InviteNaplanCoordinatorPage(driver);
		InviteNaplanCoordinatorPage.verifyInviteNaplanCoordinator(Email);
		test.log(LogStatus.PASS, "VERIFIED SENDING AN EMAIL TO A NAPLAN COORDINATOR");
		
		driver.findElement(By.xpath(".//*[@id='main']/div/a")).click();
		Thread.sleep(5000);
		ManageSchoolInformationPage ManageSchoolInformationPage = new ManageSchoolInformationPage(driver);
		ManageSchoolInformationPage.verifyManageSchoolInfo();
		test.log(LogStatus.PASS, "PERFORMED A SEARCH BY SCHOOL NAME");
		

		driver.findElement(By.xpath(".//*[@id='main']/div/a")).click();
		Thread.sleep(5000);
		SchoolPinsPage SchoolPinsPage= new SchoolPinsPage(driver);
		SchoolPinsPage.verifySchoolPins();
		test.log(LogStatus.PASS, "VERIFIED SCHOOL PIN PAGE");
		
		driver.findElement(By.xpath(".//*[@id='main']/div/a")).click();
		Thread.sleep(5000);
		MonitorSchoolProgressPage MonitorSchoolProgressPage = new MonitorSchoolProgressPage(driver);
		MonitorSchoolProgressPage.verifyMonitoringProgress();
		test.log(LogStatus.PASS, "PERFORMED A SEARCH BY SCHOOL NAME");
		
		driver.findElement(By.xpath(".//*[@id='main']/div/a")).click();
		Thread.sleep(5000);
		MonitorDevicesPage MonitorDevicesPage = new MonitorDevicesPage(driver);
		MonitorDevicesPage.verifyMonitorDevice();
		test.log(LogStatus.PASS, "PERFORMED A SEARCH FOR DEVICES BY SCHOOL NAME");
		
		driver.findElement(By.xpath(".//*[@id='main']/div/a")).click();
		Thread.sleep(5000);
		ExcludeStudentRecordPage ExcludeStudentRecordPage = new ExcludeStudentRecordPage(driver);
		ExcludeStudentRecordPage.verifyExcludeStudents();
		test.log(LogStatus.PASS, "PERFORMED A SEARCH BY SCHOOL NAME");
		
		driver.findElement(By.xpath(".//*[@id='main']/div/a")).click();
		Thread.sleep(5000);	
		ViewStudentRecordsTransferRequestPage ViewStudentRecordsTransferRequestPage = new ViewStudentRecordsTransferRequestPage(driver);
		ViewStudentRecordsTransferRequestPage.verifyRecordTransferRequest();
		test.log(LogStatus.PASS, "VERIFIED STUDENT RECORD TRANSFER PAGE");
		
		driver.findElement(By.xpath(".//*[@id='main']/div/a")).click();
		Thread.sleep(5000);	
		ExtractJurisdictionalDataPage ExtractJurisdictionalDataPage = new ExtractJurisdictionalDataPage(driver);
		ExtractJurisdictionalDataPage.verifyExtracts();
		test.log(LogStatus.PASS, "VERIFIED EXTRACT JURISDICTIONAL DATA PAGE");
		
		driver.findElement(By.xpath(".//*[@id='main']/div/a")).click();
		Thread.sleep(5000);	
		SsrAndIsrPage SsrAndIsrPage= new SsrAndIsrPage(driver);
		SsrAndIsrPage.verifySSSRandISR();
		test.log(LogStatus.PASS, "VERIFIED SSSR and ISR PAGES");
		closeSession();
}

}

