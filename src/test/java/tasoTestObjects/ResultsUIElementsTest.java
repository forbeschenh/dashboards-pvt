package tasoTestObjects;

import java.io.IOException;
import org.openqa.selenium.By;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import tasoPageObjects.TAASOAllResultsUIElementsPage;
import com.relevantcodes.extentreports.LogStatus;
import baseTestObjects.BasicTestObjects;
import baseTestObjects.ProdTestBase;

public class ResultsUIElementsTest extends BasicTestObjects {

	@Parameters({ "appURL", "TAASOUserName", "TAASOPassword" })
	@Test
	public void TAASOAllResultsUIElementsPage(String url,
			String UserName, String Password) throws InterruptedException, IOException {
		browserSetup(url);
		ProdTestBase PordTestBase = new ProdTestBase(driver);
		PordTestBase.doLogin(UserName, Password);
	    driver.findElement(By.xpath(".//*[@id='workflow-progress-results']")).click();
		Thread.sleep(4000);
		TAASOAllResultsUIElementsPage TAASOAllResultsUIElementsPage = new TAASOAllResultsUIElementsPage(driver);
		TAASOAllResultsUIElementsPage.clickResultsTab();
		TAASOAllResultsUIElementsPage.verifyChecklistItems();
		TAASOAllResultsUIElementsPage.verifyDeleiveryResultsItems();
		test.log(LogStatus.PASS, "VERIFIED ALL ELEMENTS IN THE RESULTS PAGE");
		closeSession();
}
}