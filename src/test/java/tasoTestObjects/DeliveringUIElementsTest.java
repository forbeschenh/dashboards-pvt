package tasoTestObjects;

import java.io.IOException;
import org.openqa.selenium.By;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import tasoPageObjects.TAASOAllDeliveringUIElementsPage;
import com.relevantcodes.extentreports.LogStatus;
import baseTestObjects.BasicTestObjects;
import baseTestObjects.ProdTestBase;


public class DeliveringUIElementsTest extends BasicTestObjects {

	@Parameters({ "appURL", "TAASOUserName", "TAASOPassword" })
	@Test
	public void verifyTAASOAllDeliveringUIElementsPage(String url,
			String UserName, String Password) throws InterruptedException, IOException {
		browserSetup(url);
		ProdTestBase PordTestBase = new ProdTestBase(driver);
		PordTestBase.doLogin(UserName, Password);
	    driver.findElement(By.xpath(".//*[@id='workflow-progress-delivering']")).click();
		Thread.sleep(4000);
		TAASOAllDeliveringUIElementsPage TAASOAllDeliveringUIElementsPage = new TAASOAllDeliveringUIElementsPage(driver);
		TAASOAllDeliveringUIElementsPage.verifyChecklistItems();
		TAASOAllDeliveringUIElementsPage.verifyMonitorAssessmentDeliveryItems();
		TAASOAllDeliveringUIElementsPage.verifyManageStudentInformationandRecord();
		TAASOAllDeliveringUIElementsPage.verifyReports();
		TAASOAllDeliveringUIElementsPage.verifyTestAttemptStatusGraph();
		TAASOAllDeliveringUIElementsPage.verifyProgressbyYearGraph();
		test.log(LogStatus.PASS, "VERIFIED ALL ELEMENTS IN THE DELIVERING PAGE");
		closeSession();
}
}