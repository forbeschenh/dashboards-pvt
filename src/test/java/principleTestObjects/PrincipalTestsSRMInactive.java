package principleTestObjects;

import java.io.IOException;

import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import baseTestObjects.BasicTestObjects;
import baseTestObjects.ProdTestBase;
import principlePageObjects.PrincipleDeliveringUIElementsPage;
import principlePageObjects.PrinciplePreparingUIElementsPage;
import principlePageObjects.PrincipleResultsUIElementsPage;

public class PrincipalTestsSRMInactive extends BasicTestObjects {
	
	@Parameters({ "appURL", "SRMInactive_PrincipleUserName", "SRMInactive_PrinciplePassword"})
	@Test
	public void verifyPrincipleUIElementsPage(String url,
			String UserName, String Password) throws InterruptedException, IOException {
		browserSetup(url);
		ProdTestBase PordTestBase = new ProdTestBase(driver);
		PordTestBase.doLoginbyGrid(UserName, Password);
		PrinciplePreparingUIElementsPage PrinciplePreparingUIElementsPage = new PrinciplePreparingUIElementsPage(driver);
		PrinciplePreparingUIElementsPage.verifyChecklistItems();
		PrinciplePreparingUIElementsPage.verifyStudentParticpationStatusGraph();
		PrinciplePreparingUIElementsPage.verifyPageLinksSRMInactive();
		test.log(LogStatus.PASS, "VERIFIED ALL ELEMENTS IN THE PREPARING PAGE");
		
		PrincipleDeliveringUIElementsPage PrincipleDeliveringUIElementsPage= new PrincipleDeliveringUIElementsPage(driver);
		PrincipleDeliveringUIElementsPage.verifyChecklistItems();
		PrincipleDeliveringUIElementsPage.verifyGraphs();
		PrincipleDeliveringUIElementsPage.verifyFunctionalPageLinks();
		test.log(LogStatus.PASS, "VERIFIED ALL ELEMENTS IN THE DELIVERING PAGE ");
	
		
		closeSession();
}

}
