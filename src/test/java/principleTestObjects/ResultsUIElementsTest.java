package principleTestObjects;
import java.io.IOException;

import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import principlePageObjects.PrincipleResultsUIElementsPage;
import baseTestObjects.BasicTestObjects;
import baseTestObjects.ProdTestBase;


/**
Author: dparakrama
Created : Mar 20, 2018
 */
public class ResultsUIElementsTest extends BasicTestObjects {

	@Parameters({ "appURL", "Results_PrincipleUserName", "Results_PrinciplePassword" })
	@Test
	public void verifyResultsUIElementsPage(String url,
			String UserName, String Password) throws InterruptedException, IOException {
		browserSetup(url);
		ProdTestBase PordTestBase = new ProdTestBase(driver);
		PordTestBase.doLoginbyGrid(UserName, Password);
		PrincipleResultsUIElementsPage PrincipleResultsUIElementsPage  = new PrincipleResultsUIElementsPage(driver);
		PrincipleResultsUIElementsPage.verifyPageElements();
		test.log(LogStatus.PASS, "VERIFIED ALL ELEMENTS IN THE RESULTS PAGE");
		closeSession();
}


}