package taaTestObjects;

import java.io.IOException;

import org.openqa.selenium.By;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import taaPageObjects.TAAAllPreparingUIElementsPage;
import baseTestObjects.BasicTestObjects;


import baseTestObjects.ProdTestBase;

import com.relevantcodes.extentreports.LogStatus;

public class PreparingUIElementsTest extends BasicTestObjects {
	@Parameters({ "appURL", "TAAAdminUserName", "TAAAdminPassword" })
	@Test
	public void verifyTAAAllPreparingUIElementsPage(String url,
			String UserName, String Password) throws InterruptedException, IOException {
		browserSetup(url);
		ProdTestBase PordTestBase = new ProdTestBase(driver);
		PordTestBase.doLogin(UserName, Password);
		driver.findElement(By.xpath(".//*[@id='workflow-progress-preparing']")).click();
		Thread.sleep(4000);
		TAAAllPreparingUIElementsPage TAAAllPreparingUIElementsPage = new TAAAllPreparingUIElementsPage(driver);
		TAAAllPreparingUIElementsPage.verifyChecklistItems();
		TAAAllPreparingUIElementsPage.verifyManageUsersLinksSRMInactive();
		TAAAllPreparingUIElementsPage.verifyManageJurisdictionInformation();
		TAAAllPreparingUIElementsPage.verifyReports();
		TAAAllPreparingUIElementsPage.verifySchoolTechnicalReadinessGraph();
		TAAAllPreparingUIElementsPage.verifySchoolDeliveryModeGraph();
		TAAAllPreparingUIElementsPage.verifySchoolPreparationStatusGraph();
		TAAAllPreparingUIElementsPage.verifyStudentParticipationStatusGraph();
		test.log(LogStatus.PASS, "VERIFIED ALL ELEMENTS IN THE PREPARING PAGE");
		closeSession();
		
}
}