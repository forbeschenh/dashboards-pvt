package taaTestObjects;

import java.io.IOException;

import org.openqa.selenium.By;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import taaPageObjects.TAAAllResultsUIElementsPage;
import baseTestObjects.BasicTestObjects;


import baseTestObjects.ProdTestBase;

import com.relevantcodes.extentreports.LogStatus;

public class ResultsUIElementsTest extends BasicTestObjects {

	@Parameters({ "appURL", "TAAAdminUserName", "TAAAdminPassword" })
	@Test
	public void verifyTAAAllResultsUIElementsPage(String url,
			String UserName, String Password) throws InterruptedException, IOException {
		browserSetup(url);
		ProdTestBase PordTestBase = new ProdTestBase(driver);
		PordTestBase.doLogin(UserName, Password);
		driver.findElement(By.xpath(".//*[@id='workflow-progress-results']")).click();
		Thread.sleep(4000);
		TAAAllResultsUIElementsPage TAAAllResultsUIElementsPage = new TAAAllResultsUIElementsPage(driver);
		TAAAllResultsUIElementsPage.clickResultsTab();
		TAAAllResultsUIElementsPage.verifyChecklistItems();
		TAAAllResultsUIElementsPage.verifyDeleiveryResultsItems();
		test.log(LogStatus.PASS, "VERIFIED ALL ELEMENTS IN THE RESULTS PAGE");
		closeSession();
}
}