package taaTestObjects;

import java.io.IOException;
import org.openqa.selenium.By;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import taaPageObjects.TAAAllDeliveringUIElementsPage;
import baseTestObjects.BasicTestObjects;





import baseTestObjects.ProdTestBase;

import com.relevantcodes.extentreports.LogStatus;

public class DeliveringUIElementsTest extends BasicTestObjects {

	@Parameters({ "appURL", "TAAAdminUserName", "TAAAdminPassword" })
	@Test
	public void verifyTAAAllDeliveringUIElementsPage(String url,
			String UserName, String Password) throws InterruptedException, IOException {
		browserSetup(url);
		ProdTestBase PordTestBase = new ProdTestBase(driver);
		PordTestBase.doLogin(UserName, Password);
		driver.findElement(By.xpath(".//*[@id='workflow-progress-delivering']")).click();
		Thread.sleep(4000);
		TAAAllDeliveringUIElementsPage TAAAllDeliveringUIElementsPage = new TAAAllDeliveringUIElementsPage(driver);
		TAAAllDeliveringUIElementsPage.verifyChecklistItems();
		TAAAllDeliveringUIElementsPage.verifyMonitorAssessmentDeliveryItems();
		TAAAllDeliveringUIElementsPage.verifyManageStudentInformationandRecord();
		TAAAllDeliveringUIElementsPage.verifyReports();
		TAAAllDeliveringUIElementsPage.verifyTestAttemptStatusGraph();
		TAAAllDeliveringUIElementsPage.verifyProgressbyYearGraph();
		test.log(LogStatus.PASS, "VERIFIED ALL ELEMENTS IN THE DELIVERING PAGE ");
		closeSession();
}
}