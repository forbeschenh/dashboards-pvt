package taaTestObjects;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import baseTestObjects.BasicTestObjects;
import baseTestObjects.ProdTestBase;
import commonPages.CreateStudentPage;
import commonPages.ExcludeStudentRecordPage;
import commonPages.ExtractJurisdictionalDataPage;
import commonPages.InviteBulkPrinciplesPage;
import commonPages.InviteNaplanCoordinatorPage;
import commonPages.InvitePrinciplesPage;
import commonPages.ManageSchoolInformationPage;
import commonPages.ManageStudentInformationPage;
import commonPages.ManageStudentParticipationPage;
import commonPages.ManageUsersPage;
import commonPages.MonitorDevicesPage;
import commonPages.MonitorSchoolProgressPage;
import commonPages.ReinviteBulkPrinciples;
import commonPages.SchoolPinsPage;
import commonPages.SsrAndIsrPage;
import commonPages.StudentRegistration;
import commonPages.ViewStudentRecordsTransferRequestPage;
import taaPageObjects.TAAAllDeliveringUIElementsPage;
import taaPageObjects.TAAAllPreparingUIElementsPage;
import taaPageObjects.TAAAllResultsUIElementsPage;

public class TAATestsSRMInactive extends BasicTestObjects {
	
	
	@Parameters({ "appURL", "SRMInactive_TAAAdminUserName", "SRMInactive_TAAAdminPassword", "Email"})
	@Test
	public void verifyPageNavigationAsTAA(String url,
			String UserName, String Password, String Email) throws InterruptedException, IOException {
		browserSetup(url);
		ProdTestBase PordTestBase = new ProdTestBase(driver);
		PordTestBase.doLogin(UserName, Password);
		
		
		InvitePrinciplesPage InvitePrinciplesPage = new InvitePrinciplesPage(driver);
		InvitePrinciplesPage.verifyInviteSinglePrinciple(Email);
		test.log(LogStatus.PASS, "VERIFIED SENDING AN EMAIL TO A SINGLE PRINCIPALS");
		
		
		driver.findElement(By.xpath(".//*[@id='main']/div/a")).click();
		Thread.sleep(5000);
		InviteBulkPrinciplesPage InviteBulkPrinciplesPage = new InviteBulkPrinciplesPage(driver);
		InviteBulkPrinciplesPage.verifyInvitingBulkPrinciples();
		test.log(LogStatus.PASS, "VERIFIED SENDING EMAILS TO PRINCIPALS");
		
		driver.findElement(By.xpath(".//*[@id='main']/div/a")).click();
		Thread.sleep(5000);
		ReinviteBulkPrinciples ReinviteBulkPrinciples = new ReinviteBulkPrinciples(driver);
		ReinviteBulkPrinciples.verifyReInvitingBulkPrinciples();
		test.log(LogStatus.PASS, "VERIFIED RE SENDING EMAILS TO PRINCIPALS");
		
		
		driver.findElement(By.xpath(".//*[@id='main']/div/a")).click();
		Thread.sleep(5000);
		ManageUsersPage ManageUsersPage = new ManageUsersPage(driver);
		ManageUsersPage.verifyManageUser("//a[contains(@class,\"list-group-item element-taa-supportofficer-hidden element-sector-administrator-hidden\")]");
		test.log(LogStatus.PASS, "PERFORMED A USER SEARCH BY NAME");
		
		
			
		driver.findElement(By.xpath(".//*[@id='main']/div/a")).click();
		Thread.sleep(5000);
		ManageStudentInformationPage ManageStudentInformationPage = new ManageStudentInformationPage(driver);
		ManageStudentInformationPage.verifyManageStudentInfo();
		test.log(LogStatus.PASS, "VERIFIED MANAGE USER PAGE LOADING");
			
		driver.findElement(By.xpath(".//*[@id='main']/div/a")).click();
		Thread.sleep(5000);
		CreateStudentPage CreateStudnetPage = new CreateStudentPage(driver);
		CreateStudnetPage.verifyCreateStudnetPage();
		test.log(LogStatus.PASS, "VERIFIED CREATE STUDENT PAGE LOADING");
		
		driver.findElement(By.xpath(".//*[@id='main']/div/a")).click();
		Thread.sleep(5000);
		ManageStudentParticipationPage ManageStudentParticipationPage = new ManageStudentParticipationPage(driver);
		ManageStudentParticipationPage.verifyManageStudentP();
		test.log(LogStatus.PASS, "PERFORMED A SEARCH BY SCHOOL NAME AND GROUP LEVEL");
		
		
		driver.findElement(By.xpath(".//*[@id='main']/div/a")).click();
		Thread.sleep(5000);
		InviteNaplanCoordinatorPage InviteNaplanCoordinatorPage = new InviteNaplanCoordinatorPage(driver);
		InviteNaplanCoordinatorPage.verifyInviteNaplanCoordinator(Email);
		test.log(LogStatus.PASS, "VERIFIED SENDING AN EMAIL TO A NAPLAN COORDINATOR");
		
		driver.findElement(By.xpath(".//*[@id='main']/div/a")).click();
		Thread.sleep(5000);
		ManageSchoolInformationPage ManageSchoolInformationPage = new ManageSchoolInformationPage(driver);
		ManageSchoolInformationPage.verifyManageSchoolInfo();
		test.log(LogStatus.PASS, "PERFORMED A SEARCH BY SCHOOL NAME");
		

		driver.findElement(By.xpath(".//*[@id='main']/div/a")).click();
		Thread.sleep(5000);
		SchoolPinsPage SchoolPinsPage= new SchoolPinsPage(driver);
		SchoolPinsPage.verifySchoolPins();
		test.log(LogStatus.PASS, "VERIFIED SCHOOL PIN PAGE");
		
		
		driver.findElement(By.xpath(".//*[@id='main']/div/a")).click();
		Thread.sleep(5000);
		MonitorSchoolProgressPage MonitorSchoolProgressPage = new MonitorSchoolProgressPage(driver);
		MonitorSchoolProgressPage.verifyMonitoringProgress();
		test.log(LogStatus.PASS, "PERFORMED A SEARCH BY SCHOOL NAME");
		
		driver.findElement(By.xpath(".//*[@id='main']/div/a")).click();
		Thread.sleep(5000);
		MonitorDevicesPage MonitorDevicesPage = new MonitorDevicesPage(driver);
		MonitorDevicesPage.verifyMonitorDevice();
		test.log(LogStatus.PASS, "PERFORMED A SEARCH BY SCHOOL NAME");
		
		driver.findElement(By.xpath(".//*[@id='main']/div/a")).click();
		Thread.sleep(5000);
		ExcludeStudentRecordPage ExcludeStudentRecordPage = new ExcludeStudentRecordPage(driver);
		ExcludeStudentRecordPage.verifyExcludeStudents();
		test.log(LogStatus.PASS, "PERFORMED A SEARCH BY SCHOOL NAME");
		
		driver.findElement(By.xpath(".//*[@id='main']/div/a")).click();
		Thread.sleep(5000);	
		ViewStudentRecordsTransferRequestPage ViewStudentRecordsTransferRequestPage = new ViewStudentRecordsTransferRequestPage(driver);
		ViewStudentRecordsTransferRequestPage.verifyRecordTransferRequest();
		test.log(LogStatus.PASS, "VERIFIED STUDENT RECORD TRANSFER PAGE");
		
		driver.findElement(By.xpath(".//*[@id='main']/div/a")).click();
		Thread.sleep(5000);	
		ExtractJurisdictionalDataPage ExtractJurisdictionalDataPage = new ExtractJurisdictionalDataPage(driver);
		ExtractJurisdictionalDataPage.verifyExtracts();
		test.log(LogStatus.PASS, "VERIFIED EXTRACT JURISDICTIONAL DATA PAGE");
		
		driver.findElement(By.xpath(".//*[@id='main']/div/a")).click();
		Thread.sleep(5000);	
		SsrAndIsrPage SsrAndIsrPage= new SsrAndIsrPage(driver);
		SsrAndIsrPage.verifySSSRandISR();
		test.log(LogStatus.PASS, "VERIFIED SSSR and ISR PAGES");
		driver.findElement(By.xpath(".//*[@id='main']/div/a")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath(".//*[@id='workflow-progress-preparing']")).click();
		Thread.sleep(4000);
		
		TAAAllPreparingUIElementsPage TAAAllPreparingUIElementsPage = new TAAAllPreparingUIElementsPage(driver);
		TAAAllPreparingUIElementsPage.verifyChecklistItems();
		TAAAllPreparingUIElementsPage.verifyManageUsersLinksSRMInactive();
		TAAAllPreparingUIElementsPage.verifyManageJurisdictionInformation();
		TAAAllPreparingUIElementsPage.verifyReports();
		TAAAllPreparingUIElementsPage.verifySchoolTechnicalReadinessGraph();
		TAAAllPreparingUIElementsPage.verifySchoolDeliveryModeGraph();
		TAAAllPreparingUIElementsPage.verifySchoolPreparationStatusGraph();
		TAAAllPreparingUIElementsPage.verifyStudentParticipationStatusGraph();
		test.log(LogStatus.PASS, "VERIFIED ALL ELEMENTS IN THE PREPARING PAGE");
		
		
		driver.findElement(By.xpath(".//*[@id='workflow-progress-delivering']")).click();
		Thread.sleep(4000);
		TAAAllDeliveringUIElementsPage TAAAllDeliveringUIElementsPage = new TAAAllDeliveringUIElementsPage(driver);
		TAAAllDeliveringUIElementsPage.verifyChecklistItems();
		TAAAllDeliveringUIElementsPage.verifyMonitorAssessmentDeliveryItems();
		TAAAllDeliveringUIElementsPage.verifyManageStudentInformationandRecord();
		TAAAllDeliveringUIElementsPage.verifyReports();
		TAAAllDeliveringUIElementsPage.verifyTestAttemptStatusGraph();
		TAAAllDeliveringUIElementsPage.verifyProgressbyYearGraph();
		test.log(LogStatus.PASS, "VERIFIED ALL ELEMENTS IN THE DELIVERING PAGE ");
		
		driver.findElement(By.xpath(".//*[@id='workflow-progress-results']")).click();
		Thread.sleep(4000);
		TAAAllResultsUIElementsPage TAAAllResultsUIElementsPage = new TAAAllResultsUIElementsPage(driver);
		TAAAllResultsUIElementsPage.clickResultsTab();
		TAAAllResultsUIElementsPage.verifyChecklistItems();
		TAAAllResultsUIElementsPage.verifyDeleiveryResultsItems();
		test.log(LogStatus.PASS, "VERIFIED ALL ELEMENTS IN THE RESULTS PAGE");
		
		
		closeSession();
}

	
	 public static boolean IsElementPresent(){
		 if(driver.findElements(By.xpath(".//*[@class='btn btn-primary']")).size()!= 0){
			 System.out.println("ELEMENT IS PRESENT");
			 return true;
			 }else{
			 System.out.println("ELEMENT IS ABSENT");
			 return false;
			 }
		
	
}
}
