package napcTestObjects;

import java.io.IOException;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import com.relevantcodes.extentreports.LogStatus;
import principlePageObjects.PrincipleDeliveringUIElementsPage;
import baseTestObjects.BasicTestObjects;
import baseTestObjects.ProdTestBase;

/**
Author: dparakrama
Created : Mar 20, 2018
 */
public class DeliveringUIElementsTest extends BasicTestObjects {
	@Parameters({ "appURL", "NAPcUserName", "NAPcPassword" })
	@Test
	public void verifyPrincipleDeliveringUIElementsPage(String url,
			String UserName, String Password) throws InterruptedException, IOException {
		browserSetup(url);
		ProdTestBase PordTestBase = new ProdTestBase(driver);
		PordTestBase.doLoginbyGrid1(UserName, Password);
		PrincipleDeliveringUIElementsPage PrincipleDeliveringUIElementsPage= new PrincipleDeliveringUIElementsPage(driver);
		PrincipleDeliveringUIElementsPage.verifyChecklistItems();
		PrincipleDeliveringUIElementsPage.verifyGraphs();
		PrincipleDeliveringUIElementsPage.verifyFunctionalPageLinks();
		test.log(LogStatus.PASS, "VERIFIED ALL ELEMENTS IN THE DELIVERING PAGE ");
		closeSession();
}

}