package napcTestObjects;
import java.io.IOException;

import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import principlePageObjects.PrincipleResultsUIElementsPage;
import baseTestObjects.BasicTestObjects;
import baseTestObjects.ProdTestBase;
import nacpPageObjects.NapCResultsUIElementsPage;


/**
Author: dparakrama
Created : Mar 20, 2018
 */
public class ResultsUIElementsTest extends BasicTestObjects {

	@Parameters({ "appURL", "Results_NapCUserName", "Results_NapCPassword" })
	@Test
	public void verifyResultsUIElementsPage(String url,
			String UserName, String Password) throws InterruptedException, IOException {
		browserSetup(url);
		ProdTestBase PordTestBase = new ProdTestBase(driver);
		PordTestBase.doLoginbyGrid(UserName, Password);
		NapCResultsUIElementsPage NapCResultsUIElementsPage  = new NapCResultsUIElementsPage(driver);
		NapCResultsUIElementsPage.verifyPageElements();
		test.log(LogStatus.PASS, "VERIFIED ALL ELEMENTS IN THE RESULTS PAGE");
		closeSession();
}


}