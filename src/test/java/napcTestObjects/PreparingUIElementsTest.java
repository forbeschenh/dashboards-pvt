package napcTestObjects;

import java.io.IOException;

import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import principlePageObjects.PrinciplePreparingUIElementsPage;
import baseTestObjects.BasicTestObjects;
import baseTestObjects.ProdTestBase;
import nacpPageObjects.NapCPreparingUIElementsPage;

/**
Author: dparakrama
Created : Mar 20, 2018
 */
public class PreparingUIElementsTest extends BasicTestObjects {

	@Parameters({ "appURL", "NAPcUserName", "NAPcPassword" })
	@Test
	public void verifyPrinciplePreparingUIElementsPage(String url,
			String UserName, String Password) throws InterruptedException, IOException {
		browserSetup(url);
		ProdTestBase PordTestBase = new ProdTestBase(driver);
		PordTestBase.doLoginbyGrid1(UserName, Password);
		NapCPreparingUIElementsPage NapCPreparingUIElementsPage = new NapCPreparingUIElementsPage(driver);
		NapCPreparingUIElementsPage.verifyChecklistItems();
		NapCPreparingUIElementsPage.verifyStudentParticpationStatusGraph();
		NapCPreparingUIElementsPage.verifyPageLinksSRMActive();
		test.log(LogStatus.PASS, "VERIFIED ALL ELEMENTS IN THE PREPARING PAGE");
		closeSession();
}

}

