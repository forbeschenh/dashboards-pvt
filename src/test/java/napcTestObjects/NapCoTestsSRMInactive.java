package napcTestObjects;

import java.io.IOException;

import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import baseTestObjects.BasicTestObjects;
import baseTestObjects.ProdTestBase;
import nacpPageObjects.NapCDeliveringUIElementsPage;
import nacpPageObjects.NapCPreparingUIElementsPage;
import principlePageObjects.PrincipleDeliveringUIElementsPage;
import principlePageObjects.PrinciplePreparingUIElementsPage;

public class NapCoTestsSRMInactive extends BasicTestObjects{

	
	
	@Parameters({ "appURL", "SRMInactive_NAPcoUserName", "SRMInactive_NAPcoPassword" })
	@Test
	public void verifyNAPCOUIElementsPage(String url,
			String UserName, String Password) throws InterruptedException, IOException {
		browserSetup(url);
		ProdTestBase PordTestBase = new ProdTestBase(driver);
		PordTestBase.doLoginbyGrid(UserName, Password);
		NapCPreparingUIElementsPage NapCPreparingUIElementsPage = new NapCPreparingUIElementsPage(driver);
		NapCPreparingUIElementsPage.verifyChecklistItems();
		NapCPreparingUIElementsPage.verifyStudentParticpationStatusGraph();
		NapCPreparingUIElementsPage.verifyPageLinksSRMInactive();
		test.log(LogStatus.PASS, "VERIFIED ALL ELEMENTS IN THE PREPARING PAGE");
		
		NapCDeliveringUIElementsPage NapCDeliveringUIElementsPage= new NapCDeliveringUIElementsPage(driver);
		NapCDeliveringUIElementsPage.verifyChecklistItems();
		NapCDeliveringUIElementsPage.verifyGraphs();
		NapCDeliveringUIElementsPage.verifyFunctionalPageLinks();
		test.log(LogStatus.PASS, "VERIFIED ALL ELEMENTS IN THE DELIVERING PAGE ");
		
		
		closeSession();
}
	
	
}
