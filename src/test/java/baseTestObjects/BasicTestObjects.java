package baseTestObjects;

import java.io.File;
import java.io.IOException;
import io.github.bonigarcia.wdm.ChromeDriverManager;
import io.github.bonigarcia.wdm.WebDriverManager;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;


import com.relevantcodes.extentreports.LogStatus;

import basePageObjects.BasicPageObjects;

public class BasicTestObjects extends ExtentManager {
	protected static WebDriver driver;

	/***
	 * Test Method for invoking the browser
	 * 
	 * @throws InterruptedException
	 * **/

	public void browserSetup(String Url) throws InterruptedException {
		ChromeDriverManager.getInstance().setup();
	//	WebDriverManager.chromedriver().setup();
		getInstance();
		test = report.startTest((this.getClass().getSimpleName()));
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get(Url);
		test.log(LogStatus.PASS, "BROWSER SESSION STARTED");
		Thread.sleep(5000);

	}

	public void closeSession() {
		report.endTest(test);
		report.flush();
		driver.quit();
		test.log(LogStatus.PASS, "TEST ENDED");
	}

	
	public void takeAScreenShot (String FileName) throws IOException {
		File src = new File("../scis-regression/"+FileName);
		src.delete();
		src = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(src, new File(
				"../screenshots/"+FileName));
		
	}
	
	



	
	}
	

