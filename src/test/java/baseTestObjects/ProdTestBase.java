package baseTestObjects;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import basePageObjects.LoginPage;

public class ProdTestBase {
	private WebDriver driver;

	public ProdTestBase(WebDriver driver) {
		this.driver = driver;
	}

	public void doLogin(String UserName, String Password)
			throws InterruptedException, IOException {

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		LoginPage loginPage = new LoginPage(driver);
		loginPage.loginToTheApplication(UserName, Password);
		System.out.println("Enter Passcode");
		String Passcode = br.readLine();
		driver.findElement(By.xpath("//*[@id='TFAPasscode']")).sendKeys(
				Passcode);
		Thread.sleep(3000);
		driver.findElement(
				By.xpath("//*[@id='content']/div[2]/div/form/footer/button/span[1]"))
				.click();
		Thread.sleep(3000);

	}

	
	
	public void doLoginbyGrid (String UserName, String Password)
			throws InterruptedException, IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		LoginPage loginPage = new LoginPage(driver);
		loginPage.loginToTheApplication(UserName, Password);
		
		
		String g1 = driver.findElement(By.xpath("//*[@id=\"content\"]/div[2]/div/form/div[2]/div/div[1]")).getText();
		int n1 = Integer.parseInt(driver.findElement(By.xpath("//*[@id=\"content\"]/div[2]/div/form/div[2]/div/div[2]")).getText());
		String g2 = driver.findElement(By.xpath("//*[@id=\"content\"]/div[2]/div/form/div[2]/div/div[4]")).getText();
		int n2 = Integer.parseInt(driver.findElement(By.xpath("//*[@id=\"content\"]/div[2]/div/form/div[2]/div/div[5]")).getText());
		String g3 = driver.findElement(By.xpath("//*[@id=\"content\"]/div[2]/div/form/div[2]/div/div[7]")).getText();
		int n3 = Integer.parseInt(driver.findElement(By.xpath("//*[@id=\"content\"]/div[2]/div/form/div[2]/div/div[8]")).getText());
		
		
		String Filepath = "\\src\\test\\resources\\Data.xlsx";
		String curDir = System.getProperty("user.dir");
		File exfl = new File(curDir + Filepath);
		FileInputStream fin = new FileInputStream(exfl);
		XSSFWorkbook wb = new XSSFWorkbook(fin);
		XSSFSheet sheet1 = wb.getSheetAt(0);
		
		
		
		String code = sheet1.getRow(n1).getCell(doLoginbyGrid2(g1)).getStringCellValue() + sheet1.getRow(n2).getCell(doLoginbyGrid2(g2)).getStringCellValue() + sheet1.getRow(n3).getCell(doLoginbyGrid2(g3)).getStringCellValue();
		driver.findElement(By.xpath(".//*[@id='TFAPasscode_0']")).sendKeys(code.substring(0,1));
		driver.findElement(By.xpath(".//*[@id='TFAPasscode_1']")).sendKeys(code.substring(1,2));
		driver.findElement(By.xpath(".//*[@id='TFAPasscode_2']")).sendKeys(code.substring(2,3));
		driver.findElement(By.xpath(".//*[@id='TFAPasscode_3']")).sendKeys(code.substring(3,4));
		driver.findElement(By.xpath(".//*[@id='TFAPasscode_4']")).sendKeys(code.substring(4,5));
		driver.findElement(By.xpath(".//*[@id='TFAPasscode_5']")).sendKeys(code.substring(5,6));
		
		Thread.sleep(3000);
		driver.findElement(
				By.xpath("//*[@id='content']/div[2]/div/form/footer/button/span[1]"))
				.click();
		Thread.sleep(3000);
	}

	public void doLoginbyGrid1 (String UserName, String Password)
			throws InterruptedException, IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		LoginPage loginPage = new LoginPage(driver);
		loginPage.loginToTheApplication(UserName, Password);
		System.out
				.println("ENTER THE DIGIT ONE OF THE 1ST PAIR]");
		String d1 = br.readLine();
		driver.findElement(By.xpath(".//*[@id='TFAPasscode_0']")).sendKeys(d1);
		System.out
				.println("ENTER THE DIGIT TWO OF THE 1ST PAIR]");
		String d2 = br.readLine();
		driver.findElement(By.xpath(".//*[@id='TFAPasscode_1']")).sendKeys(d2);

		System.out
				.println("ENTER THE DIGIT ONE OF THE 2ND PAIR]");
		String d3 = br.readLine();
		driver.findElement(By.xpath(".//*[@id='TFAPasscode_2']")).sendKeys(d3);
		System.out
				.println("ENTER THE DIGIT TWO OF THE 2ND PAIR]");
		String d4 = br.readLine();
		driver.findElement(By.xpath(".//*[@id='TFAPasscode_3']")).sendKeys(d4);
		System.out
				.println("ENTER THE DIGIT ONE OF THE 3RD PAIR]");
		String d5 = br.readLine();
		driver.findElement(By.xpath(".//*[@id='TFAPasscode_4']")).sendKeys(d5);
		System.out
				.println("ENTER THE DIGIT TWO OF THE 3RD PAIR]");
		String d6 = br.readLine();
		driver.findElement(By.xpath(".//*[@id='TFAPasscode_5']")).sendKeys(d6);
		Thread.sleep(3000);
		driver.findElement(
				By.xpath("//*[@id='content']/div[2]/div/form/footer/button/span[1]"))
				.click();
		Thread.sleep(3000);
}
	
	public int doLoginbyGrid2 (String value)
			throws InterruptedException, IOException {
		
		switch (value){
		
		case "A": return 1; 
		
		case "B": return 2; 
			
		case "C": return 3; 
		
		case "D": return 4; 
		
		case "E": return 5; 
		
		case "F": return 6; 
		
		case "G": return 7; 
		
		case "H": return 8; 
		
		case "I": return 9; 
		
		case "J": return 10; 
			
		default:
			System.out.println("GRID VALUE NOT FOUND]");
			return 0;
		

			
		}

		
	
	}
	
}